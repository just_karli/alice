#pragma once
#ifndef PSEUDO_RANDOM_H_
#define PSEUDO_RANDOM_H_
#include <cstdint>
#include <glm/glm.hpp>

namespace Random
{
	float HaltonSample(uint64_t sampleIndex, uint32_t base);
	glm::vec2 HaltonSample2D(uint64_t sampleIndex, uint32_t base1, uint32_t base2);
	glm::vec2 HaltonSampleRing(uint64_t sampleIndex, uint32_t base);
	glm::vec2 HaltonSampleDisc(uint64_t sampleIndex, uint32_t base1, uint32_t base2);
	glm::vec3 HaltonSampleHemisphere(uint64_t sampleIndex, uint32_t base1, uint32_t base2);

	uint64_t XorShift();

}

#endif 
