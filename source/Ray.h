#ifndef RAY_H_
#define RAY_H_

#include <glm/glm.hpp>

struct Ray
{
	glm::vec3 position;
	glm::vec3 direction;

	Ray() = default;
	Ray(const glm::vec3& pos, const glm::vec3& dir);
	glm::vec3 evaluate(float t) const;
};

#endif 