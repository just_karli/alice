#ifndef CAMERA_H_
#define CAMERA_H_

#pragma once
#include <glm/glm.hpp>
#include "Ray.h"

class Camera
{
public:
	Camera(const glm::vec3 _position, const glm::vec3 lookAt, const float verticalFov, const float aspect, float focusDistance = 1, float aperture = 0);
	Ray getRay(const glm::vec2 uv, const glm::vec2 offset) const;

private:
	glm::vec3 position;
	glm::vec3 lowerLeftCorner;

	glm::vec3 horizontal;
	glm::vec3 vertical;

	glm::vec3 m_right, m_up, m_forward;

	float m_aperture = 0.0f;
	float focalLength = 0.0f;
};

#endif 
