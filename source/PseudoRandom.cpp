#include "PseudoRandom.h"
#include <chrono>
#include <glm/gtc/constants.inl>

float Random::HaltonSample(uint64_t sampleIndex, uint32_t base)
{
	float result = 0.0f;
	float f = 1.0f;

	while(sampleIndex > 0)
	{
		f = f / base;
		result += f * (sampleIndex % base);
		sampleIndex = sampleIndex / base;
	}

	return result;
}

glm::vec2 Random::HaltonSample2D(uint64_t sampleIndex, uint32_t base1, uint32_t base2)
{
	return glm::vec2(HaltonSample(sampleIndex, base1), HaltonSample(sampleIndex, base2));
}

glm::vec2 Random::HaltonSampleRing(uint64_t sampleIndex, uint32_t base)
{
	float theta = 2.0f * glm::pi<float>() * HaltonSample(sampleIndex, base);
	return glm::vec2(std::cos(theta), std::sin(theta));
}

glm::vec2 Random::HaltonSampleDisc(uint64_t sampleIndex, uint32_t base1, uint32_t base2)
{
	float theta = 2.0f * glm::pi<float>() * HaltonSample(sampleIndex, base1);
	float r = HaltonSample(sampleIndex, base2);

	return glm::vec2(
		r * std::cos(theta),
		r * std::sin(theta)
	);
}

glm::vec3 Random::HaltonSampleHemisphere(uint64_t sampleIndex, uint32_t base1, uint32_t base2)
{
	const float u1 = HaltonSample(sampleIndex, base1);
	const float u2 = HaltonSample(sampleIndex, base2);

	const float r = std::sqrt(1.0f - u1 * u1);
	const float phi = 2.0f * glm::pi<float>() * u2;
	return glm::vec3(
		r * std::cos(phi),
		r * std::sin(phi),
		u1
	);
}

uint64_t Random::XorShift()
{
	static auto startTime = std::chrono::high_resolution_clock::now();
	const auto timeNow = std::chrono::high_resolution_clock::now();

	const std::chrono::duration<uint64_t, std::nano> duration = (timeNow - startTime);
	uint64_t x = duration.count();
	x ^= x >> 13;
	x ^= x << 15;
	x ^= x >> 17;

	return x;
}
