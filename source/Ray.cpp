#include "Ray.h"

Ray::Ray(const glm::vec3& pos, const glm::vec3& dir) : position(pos), direction(dir)
{
}

glm::vec3 Ray::evaluate(float t) const
{
	return position + t * direction; 
}
