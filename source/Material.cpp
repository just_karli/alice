#include "Material.h"

bool Lambertian::scatter(const Ray& ray, const HitRecord& hitrecord, glm::vec3& attenuation, Ray& scattered) const
{
	{
		const glm::vec3 target = hitrecord.position + hitrecord.normal + Random::HaltonSampleHemisphere(m_sampleIndex++, 5, 7);

		scattered = Ray(hitrecord.position, target - hitrecord.position);
		attenuation = albedo;

		return true;
	}
}

bool Metal::scatter(const Ray& ray, const HitRecord& hitrecord, glm::vec3& attenuation, Ray& scattered) const
{
	const glm::vec3 reflected = glm::reflect(glm::normalize(ray.direction), glm::normalize(hitrecord.normal));
	scattered = Ray(hitrecord.position, glm::normalize(reflected + m_roughness * Random::HaltonSampleHemisphere(m_sampleIndex++, 5, 7)));
	attenuation = albedo;
	return (glm::dot(scattered.direction, hitrecord.normal) > 0);
}

bool Dielectric::scatter(const Ray& ray, const HitRecord& hitrecord, glm::vec3& attenuation, Ray& scattered) const
{
	const glm::vec3 reflectDir = glm::reflect(glm::normalize(ray.direction), glm::normalize(hitrecord.normal));
	attenuation = glm::vec3(1);

	glm::vec3 outwardNormal;
	float niOverNt;
	float cosine;
	if (glm::dot(ray.direction, hitrecord.normal) > 0)
	{
		outwardNormal = -hitrecord.normal;
		niOverNt = refractionIndex;
		cosine = refractionIndex * glm::dot(glm::normalize(ray.direction), glm::normalize(hitrecord.normal));
	}
	else
	{
		outwardNormal = hitrecord.normal;
		niOverNt = 1 / refractionIndex;
		cosine = -glm::dot(glm::normalize(ray.direction), glm::normalize(hitrecord.normal));
	}

	glm::vec3 refractDir;
	float reflect_prob = 0.0f;

	if (refract(ray.direction, outwardNormal, niOverNt, refractDir))
		reflect_prob = schlick(cosine, refractionIndex);
	else
		reflect_prob = 1.0f;

	if (Random::HaltonSample(m_sampleIndex++, 7) < reflect_prob)
		scattered = Ray(hitrecord.position, glm::normalize(reflectDir));
	else
		scattered = Ray(hitrecord.position, glm::normalize(refractDir));

	return true;
}

bool Dielectric::refract(const glm::vec3& v, const glm::vec3& n, const float niOverNt, glm::vec3& refracted) const
{
	const glm::vec3 uv = glm::normalize(v);
	const float dt = glm::dot(uv, n);

	const float discriminant = 1.0f - niOverNt * niOverNt * (1 - dt * dt);
	if (discriminant > 0)
	{
		refracted = niOverNt * (uv - n * dt) - n * glm::sqrt(discriminant);
		return true;
	}

	return false;
}

float Dielectric::schlick(const float cosine, const float refrIdx) const
{
	float r0 = (1 - refrIdx) / (1 + refrIdx);
	r0 = r0 * r0;
	return r0 + (1 - r0) * glm::pow(1 - cosine, 5);
}