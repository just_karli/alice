#include "Camera.h"

Camera::Camera(const glm::vec3 _position, const glm::vec3 lookAt, const float verticalFov, const float aspect, float focusDistance, float aperture) :
	position(_position), 
	m_aperture(aperture),
	focalLength(focusDistance)
{
	float theta = glm::radians(verticalFov);

	const float halfHeight = glm::tan(theta * 0.5f);
	const float halfWidth = aspect * halfHeight;

	m_forward = glm::normalize(lookAt - position);
	m_right = glm::normalize(glm::cross(glm::vec3(0, 1, 0), m_forward));
	m_up = glm::cross(m_right, m_forward);


	lowerLeftCorner = position + 1.0f * m_forward;
	horizontal = halfWidth * m_right;
	vertical = halfHeight * m_up;
}

Ray Camera::getRay(const glm::vec2 uv, const glm::vec2 offset) const
{
	const glm::vec2 rd = m_aperture * 0.5f * offset;
	glm::vec2 ndc = glm::vec2(2.0 * uv.x - 1.0f, -2.0f * uv.y + 1.0f);
	
	glm::vec3 p = lowerLeftCorner + ndc.x * horizontal + ndc.y * vertical;
	glm::vec3 focalPoint = position + focalLength * glm::normalize(p - position);

	const glm::vec3 origin = position + rd.x * horizontal + rd.y * vertical;

	return Ray(origin, glm::normalize(focalPoint - origin));
}