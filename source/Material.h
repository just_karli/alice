#ifndef MATERIAL_H_
#define MATERIAL_H_
#pragma once

#include <glm/glm.hpp>
#include "HitRecord.h"
#include "Ray.h"
#include "PseudoRandom.h"

class Material
{
public:
	virtual ~Material() = default;
	virtual bool scatter(const Ray& ray, const HitRecord& hitrecord, glm::vec3& attenuation, Ray& scattered) const = 0;

protected:
	mutable uint64_t m_sampleIndex = 0u;
};

class Lambertian : public Material
{
public:
	Lambertian() = default;
	Lambertian(const glm::vec3& color) : albedo(color) { }

	bool scatter(const Ray& ray, const HitRecord& hitrecord, glm::vec3& attenuation, Ray& scattered) const override;

private:
	glm::vec3 albedo = glm::vec3(0.78f);
};

class Metal : public Material
{
public:
	Metal(const glm::vec3& color, const float roughness) : albedo(color), m_roughness(roughness) { }
	bool scatter(const Ray& ray, const HitRecord& hitrecord, glm::vec3& attenuation, Ray& scattered) const override;

private:
	glm::vec3 albedo;
	float m_roughness;
};

class Dielectric : public Material
{
public:
	Dielectric(const float refractIdx) : refractionIndex(refractIdx) { }

	bool scatter(const Ray& ray, const HitRecord& hitrecord, glm::vec3& attenuation, Ray& scattered) const override;
	bool refract(const glm::vec3& v, const glm::vec3& n, const float niOverNt, glm::vec3& refracted) const;

	float schlick(const float cosine, const float refrIdx) const;

private:
	float refractionIndex;
};

#endif 
