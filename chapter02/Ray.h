#ifndef RAY_H_
#define RAY_H_

#include <glm/glm.hpp>

struct Ray
{
	Ray() = default;
	Ray(const glm::vec3& pos, const glm::vec3& dir) : position(pos), direction(dir) {}
	glm::vec3 position;
	glm::vec3 direction;

	glm::vec3 Evaluate(float t) const { return position + t * direction; }
};

#endif 