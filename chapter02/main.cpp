#include <iostream>
#include <glm/glm.hpp>
#include "spdlog/spdlog.h"

#include "Ray.h"

// returns blue environment background dependent on the y direction
glm::vec3 Trace(const Ray& r)
{
	glm::vec3 unit_dir = glm::normalize(r.direction);
	float t = 0.5f * (unit_dir.y + 1.0f);
	return (1.0f - t) * glm::vec3(1.0f) + t * glm::vec3(0.5f, 0.7f, 1.0f);
}

/**
* Chapter 2: Output an image to stdout using glm & ray implementation
* Print an rgb image in the ppm format
*/
int main(int argc, char** argv)
{
	auto console = spdlog::stdout_color_mt("console");
	int nx = 200;
	int ny = 100;

	// the ppm header 
	// format name, width, height, #colors
	std::cout << "P3\n" << nx << " " << ny << std::endl;
	std::cout << 255 << std::endl;

	glm::vec3 lowerLeftCorner(-2.0f, -1.0f, -1.0f);
	glm::vec3 horizontal(4.0f, 0.0f, 0.0f);
	glm::vec3 vertical(0.0f, 2.0f, 0.0f);
	glm::vec3 origin(0.0f);

	for (int j = ny - 1; j >= 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			float u = float(i) / float(nx);
			float v = float(j) / float(ny);
			const glm::vec3 direction = lowerLeftCorner + u * horizontal + v * vertical;
			const Ray ray(origin, direction);

			const glm::vec3 col = Trace(ray);

			int ir = int(255.99 * col.r);
			int ig = int(255.99 * col.g);
			int ib = int(255.99 * col.b);

			std::cout << ir << " " << ig << " " << ib << std::endl;
		}
	}

	return 0;
}