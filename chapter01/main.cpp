#include <iostream>
#include "spdlog/spdlog.h"

/**
 * Chapter 1: Output an image to stdout
 * Print an rgb image in the ppm format
 */
int main(int argc, char** argv)
{
	auto console = spdlog::stdout_color_mt("console");
	int nx = 200;
	int ny = 100;

	// the ppm header 
	// format name, width, height, #colors
	std::cout << "P3\n" << nx << " " << ny << std::endl;
	std::cout << 255 << std::endl;

	for(int j = ny - 1; j >= 0; j--)
	{
		for(int i = 0; i < nx; i++)
		{
			float r = float(i) / float(nx);
			float g = float(j) / float(ny);
			float b = 0.2f;

			int ir = int(255.99 * r);
			int ig = int(255.99 * g);
			int ib = int(255.99 * b);
			
			std::cout << ir << " " << ig << " " << ib << std::endl;
		}
	}

	return 0;
}