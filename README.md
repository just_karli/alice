# Alice 
Alice is a ray tracer based on the kindle book series: Ray tracing in a .. by Peter Shirley

* [Amazon](https://www.amazon.de/Tracing-Weekend-Minibooks-Book-English-ebook/dp/B01B5AODD8/ref=pd_sim_351_1?_encoding=UTF8&psc=1&refRID=DVY4VJ0PGD0R8FEMHSZJ)

## Usage
Use cmake out of source build to generate a solution file for your IDE

	mkdir build
	cd build 
	cmake .. "-G Visual Studio 15 2017 Win64"

## build 
	cd build
	cmake --build . 

## clean 
	cd build
	cmake --build . --target clean

## run tests
	- do the build step 
	ctest -C <BUILD_TYPE> --output-on-failure

## how to run & see results of the Alice Raytracer
	There is an executable for nearly every chapter of the book, showing the progress of the ray tracer. The ray tracer prints an image in the [ppm](https://en.wikipedia.org/wiki/Netpbm_format) format to stdout.
	To execute a chapter, build the project & pipe the result of the executable to a ppm file and watch it via a ppm viewer. 

	./build/Debug/chapter01.exe >> out/chapter01.ppm

## install
	cd build
	cmake --build . --target install

## Requirements 
* cmake 3.8.2

## External Dependencies
	* [glm](https://github.com/g-truc/glm.git)
	* [spdlog](https://github.com/gabime/spdlog)
	* [Catch](https://github.com/philsquared/Catch)


