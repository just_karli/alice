#ifndef HIT_RECORD_H_
#define HIT_RECORD_H_

#pragma once

#include <glm/glm.hpp>
struct Material;

struct HitRecord
{
	~HitRecord() { material = nullptr; }
	float t = 0.0f;
	glm::vec3 position;
	glm::vec3 normal;
	Material* material = nullptr;
};


#endif
