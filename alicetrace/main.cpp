#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <chrono>
#include <thread>
#include <imgui.h>
#include "imgui_impl_glfw.h"
#include "../renderlib/Texture.h"
#include "../renderlib/RenderUtil.h"
#include "../renderlib/FullscreenQuad.h"
#include "../source/Hitable.h"
#include "../source/Sphere.h"
#include "../source/Camera.h"
#include "../renderlib/ImageLoader.h"
#include <atomic>
#include <mutex>
#include "../source/PseudoRandom.h"
#include <random>
#include "WriteableImage.h"
#include "Image.h"

GLFWwindow* _glfwWindow = nullptr;
GLuint _globalVao = 0;
constexpr int MAX_FRAME_CAP = 5000;

int _width = 0;
int _height = 0;

int _rayCount = 0;

WriteableImage writeableImage;
Texture* m_renderTexture = nullptr;
Texture* m_fallbackTexture = nullptr;
int _imageWidth = 0;
int _imageHeight = 0;
int _nrSamples = 0;

FullscreenQuad m_screen;
ShaderProgram* m_program = nullptr;
Camera* camera = nullptr;

void InitializeUi()
{
	ImGui::CreateContext();
	ImGui_ImplGlfwGL3_Init(_glfwWindow, true);

	ImGui::StyleColorsDark();
}

void InitializeRaytracer()
{
	_imageWidth = 512;
	_imageHeight = 384;
	_nrSamples = 16;

	writeableImage.resize(_imageWidth, _imageHeight);

	m_screen.initialize();

	m_program = new ShaderProgram;
	m_program->loadVertexShader("res/shader/simpleFullscreen.vs")
		->loadFragmentShader("res/shader/simpleFullscreen.fs")
		->compile();

	const glm::vec3 lookPosition = glm::vec3(-2, 2, 1);
	const glm::vec3 lookAt = glm::vec3(0, 0, -1);

	const float distanceToFocus = glm::length(lookPosition - lookAt);
	const float aperture = 2.0f;

	camera = new Camera(lookPosition, lookAt, 90.0f, _imageWidth / static_cast<float>(_imageHeight));
	m_fallbackTexture = Texture::loadTexture(ImageLoader::GetInstance().loadImage("res/uvgrid.bmp"));
}

void glfwWindowResize(GLFWwindow* window, int width, int height)
{
	_width = width;
	_height = height;
}

bool InitializeGLContext(const std::string& title, int width, int height)
{
	if (!glfwInit())
	{
		std::cout << "Failed to initialize glfw" << std::endl;
		return false;
	}

#if _DEBUG
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif 

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, false);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_REFRESH_RATE, GLFW_DONT_CARE);

	_glfwWindow = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);

	glewExperimental = true;
	if (_glfwWindow == nullptr)
	{
		std::cout << "Failed to initialize glfw Window" << std::endl;
		return false;
	}

	glfwSetWindowSizeCallback(_glfwWindow, glfwWindowResize);

	glfwMakeContextCurrent(_glfwWindow);
	glfwSwapInterval(1);

	if (glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialize GLEW" << std::endl;
		return false;
	}

	glGenVertexArrays(1, &_globalVao);
	glBindVertexArray(_globalVao);
	return true;
}

bool Initialize(const std::string& title, int width, int height)
{
	if (!InitializeGLContext(title, width, height))
		return false;

	InitializeUi();

	InitializeRaytracer();

	return true;
}
std::vector<std::pair<Ray, int>> GenerateRays();

void ExecuteMainLoop();
void Update(float deltaSeconds);
void Render();
void Release();

std::vector<std::unique_ptr<Hitable>> GenerateRandomScene();

void Trace(uint32_t start, uint32_t end, uint32_t threadnum, void* data);
glm::vec3 GetHitColor(const Ray&r, std::vector<std::unique_ptr<Hitable>>& world, const int depth, int& inoutRaycount);

std::vector<std::unique_ptr<Hitable>> world = GenerateRandomScene();
//std::vector<std::unique_ptr<Hitable>> world;

std::mutex targetMutex;

int main(char** argc, int argv)
{
	std::cout << "Hello AliceTrace" << std::endl;

	_width = 800;
	_height = 600;

	if (Initialize("AliceTrace", _width, _height))
		ExecuteMainLoop();

	Release();
	std::cout << "Exiting AliceTrace" << std::endl;
	return 0;
}

void ExecuteMainLoop()
{
	int frames = 0;
	std::chrono::milliseconds frameCounter = std::chrono::milliseconds::zero();

	const double frameTime = 1.0f / MAX_FRAME_CAP;

	std::chrono::high_resolution_clock::time_point lastTime = std::chrono::high_resolution_clock::now();
	double unprocessedTime = 0.0f;

	// generate rays 
	std::vector<std::pair<Ray, int>> rayBuffer = GenerateRays();
	
	// create task 

	do
	{
		bool render = false;
		const std::chrono::high_resolution_clock::time_point startTime = std::chrono::high_resolution_clock::now();
		std::chrono::milliseconds elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(startTime - lastTime);
		lastTime = startTime;

		unprocessedTime += elapsed.count() / static_cast<float>(1000.0f);
		frameCounter += elapsed;

		while(unprocessedTime > frameTime)
		{
			render = true;
			unprocessedTime -= frameTime;
			const float deltaSeconds = elapsed.count() / static_cast<float>(1000.0f);

			Update(deltaSeconds);

			if(frameCounter.count() >= 1000.0f)
			{
				frames = 0;
				frameCounter = std::chrono::milliseconds::zero();
			}
		}

		if(render)
		{
			ImGui_ImplGlfwGL3_NewFrame();

			Render();

			ImGui::Text("%.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
			ImGui::Text("Window size: [%d, %d]", _width, _height);
			ImGui::Text("Image size: [%d, %d] #%d", _imageWidth, _imageHeight, _nrSamples);
			ImGui::Text("Rays cast: %d", _rayCount);
			ImGui::Render();
			ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());

			glfwSwapBuffers(_glfwWindow);
			glfwPollEvents();
			frames++;
		} 
		else
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
		}


	} while (glfwGetKey(_glfwWindow, GLFW_KEY_ESCAPE) != GLFW_PRESS && !glfwWindowShouldClose(_glfwWindow));
}

void Update(float deltaSeconds)
{
}

void Render()
{
	if(writeableImage.hasChanged())
	{
		delete m_renderTexture;
		m_renderTexture = Texture::loadTexture(writeableImage.image.data, writeableImage.imageDescription);
		writeableImage.resetChanged();
	}

	const int windowWidth = _width;
	const int windowHeight = _height;

	const RectF viewRect = RenderUtil::createFittingRectangle(glm::vec2(writeableImage.getWidth(), writeableImage.getHeight()), glm::vec2(windowWidth, windowHeight));
	RenderUtil::setViewport(viewRect);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClearColor(0, 0, 1, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_program->bind();
	if(m_renderTexture != nullptr)
	{
		m_program->setTexture("inputTexture", m_renderTexture, 0);
		m_screen.draw(m_program);
	}
	m_program->unbind();
}

void Release()
{
	ImGui_ImplGlfwGL3_Shutdown();
	ImGui::DestroyContext();

	glDeleteVertexArrays(1, &_globalVao);
	_globalVao = 0;

	delete m_program;
	delete m_renderTexture;
	delete m_fallbackTexture;

	delete camera;

	glfwDestroyWindow(_glfwWindow);
	_glfwWindow = nullptr;
	glfwTerminate();
}

std::vector<std::unique_ptr<Hitable>> GenerateRandomScene()
{
	int n = 500;
	std::vector<std::unique_ptr<Hitable>> hitableList;
	hitableList.push_back(std::make_unique<Sphere>(glm::vec3(0, -1000, 0), 1000, new Lambertian(glm::vec3(0.5, 0.5, 0.5))));

	static std::random_device device;
	static std::ranlux24_base generator(device());
	static std::uniform_real_distribution<float> uniformDist(0.f, 1.f);

	int i = 1;
	for (int a = -11; a < 11; a++)
	{
		for (int b = -11; b < 11; b++)
		{
			const float chooseMaterial = uniformDist(generator);
			glm::vec3 center(
				a + 0.9 * uniformDist(generator),
				0.2,
				b + 0.9 * uniformDist(generator)
			);

			if (glm::length(center - glm::vec3(4, 0.2, 0)) > 0.9f)
			{
				if (chooseMaterial < 0.8f)
				{
					// diffuse
					hitableList.push_back(std::make_unique<Sphere>(center, 0.2, new Lambertian(
						glm::vec3(
							uniformDist(generator) * uniformDist(generator),
							uniformDist(generator) * uniformDist(generator),
							uniformDist(generator) * uniformDist(generator))
					)));
				}
				else if (chooseMaterial < 0.95f)
				{
					// metal
					hitableList.push_back(std::make_unique<Sphere>(center, 0.2, new Metal(
						glm::vec3(
							0.5f * (1 + uniformDist(generator)),
							0.5f * (1 + uniformDist(generator)),
							0.5f * (1 + uniformDist(generator))
						),
						0.5f * uniformDist(generator)
					)));

				}
				else
				{
					// glass
					hitableList.push_back(std::make_unique<Sphere>(center, 0.2, new Dielectric(1.5f)));
				}
			}
		}
	}

	hitableList.push_back(std::make_unique<Sphere>(glm::vec3(0, 1, 0), 1.0, new Dielectric(1.5f)));
	hitableList.push_back(std::make_unique<Sphere>(glm::vec3(-4, 1, 0), 1.0, new Lambertian(glm::vec3(0.4, 0.2, 0.1))));
	hitableList.push_back(std::make_unique<Sphere>(glm::vec3(4, 1, 0), 1.0, new Metal(glm::vec3(0.7, 0.6, 0.5), 0.0f)));
	return hitableList;
}

std::vector<std::pair<Ray, int>> GenerateRays()
{
	std::vector<std::pair<Ray, int>> rays;
	rays.reserve(_imageWidth * _imageHeight * _nrSamples);
	
	int rayIndex = 0;
	for (uint32_t j = 0; j < _imageHeight; ++j)
	{
		for (int i = 0; i < _imageWidth; i++)
		{
			glm::vec3 col(0.0f);
			for (int s = 0; s < _nrSamples; s++)
			{
				glm::vec2 jitterOffset = Random::HaltonSample2D(s, 2, 3);
				float u = float(i + jitterOffset.x) / float(_imageWidth);
				float v = float(j + jitterOffset.y) / float(_imageHeight);

				const glm::vec2 offset = Random::HaltonSampleDisc(s + i + j, 4, 5);
				
				Ray r = camera->getRay(glm::vec2(u, v), offset);
				rays.push_back(std::make_pair(r, rayIndex++));
				//col += GetHitColor(r, world, 0, rayIndex);
			}

			/*col /= float(_nrSamples);*/
			//jobdata.target->writePixel(i, j, col);
		}
	}

	return rays;
}

// returns blue environment background dependent on the y direction if there was no hit with a world object
// else it will return the surface normal of the object
glm::vec3 GetHitColor(const Ray& ray, std::vector<std::unique_ptr<Hitable>>& world, const int depth, int& inoutRayCount)
{
	HitRecord nearestHit;
	float closestHitDistance = std::numeric_limits<float>::max();
	bool hitAnything = false;
	++inoutRayCount;
	for (const auto& worldObject : world)
	{
		if (worldObject->intersect(ray, 0.01f, closestHitDistance, nearestHit))
		{
			hitAnything = true;
			closestHitDistance = nearestHit.t;
		}
	}

	if (hitAnything)
	{
		Ray scatteredRay;
		glm::vec3 attenuation;
		if (depth < 50 && nearestHit.material->scatter(ray, nearestHit, attenuation, scatteredRay))
		{
			return attenuation * GetHitColor(scatteredRay, world, depth + 1, inoutRayCount);
		}

		return glm::vec3(0.0f);
	}

	const glm::vec3 unit_dir = glm::normalize(ray.direction);
	float t = 0.5f * (unit_dir.y + 1.0f);
	return (1.0f - t) * glm::vec3(1.0f) + t * glm::vec3(0.5f, 0.7f, 1.0f);
}
