#include "WriteableImage.h"

WriteableImage::WriteableImage()
{
	imageDescription.channels = 4;
	imageDescription.imageType = 1;
	imageDescription.mipmapEnabled = false;
}

void WriteableImage::writePixel(unsigned int x, unsigned int y, unsigned char color)
{
	assert(x < imageDescription.width);
	assert(y < imageDescription.height);

	const int stride = imageDescription.width * 4;	// width * #channels
	const int yIndex = stride * y;
	const int xIndex = 4 * x;	// x * #channels

	image.data.at(yIndex + xIndex) = color;

	m_hasChanged = true;
}

void WriteableImage::writePixel(unsigned int x, unsigned int y, glm::vec3 color)
{
	assert(x < imageDescription.width);
	assert(y < imageDescription.height);

	const int stride = imageDescription.width * 4;	// width * #channels
	const int yIndex = stride * y;
	const int xIndex = 4 * x;	// x * #channels

	const int index = xIndex + yIndex;

	image.data.at(index) = static_cast<unsigned char>(color.x * 255);
	image.data.at(index + 1) = static_cast<unsigned char>(color.y * 255);
	image.data.at(index + 2) = static_cast<unsigned char>(color.z * 255);
	image.data.at(index + 3) = static_cast<unsigned char>(255);

	m_hasChanged = true;
}

void WriteableImage::clear()
{
	image.data.clear();
	m_hasChanged = true;
}

void WriteableImage::resize(unsigned int width, unsigned int height)
{
	imageDescription.width = width;
	imageDescription.height = height;

	clear();

	image.data.resize(width * height * 4, 0);
	m_hasChanged = true;
}

bool WriteableImage::hasChanged() const
{
	return m_hasChanged;
}

void WriteableImage::resetChanged()
{
	m_hasChanged = false;
}

int WriteableImage::getHeight() const
{
	return imageDescription.height;
}

int WriteableImage::getWidth() const 
{
	return imageDescription.width;
}
