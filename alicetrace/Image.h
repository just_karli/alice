#pragma once
#ifndef ALICE_IMAGE_H_
#define ALICE_IMAGE_H_
#include <vector>

struct ImageBlob
{
	std::vector<uint8_t> data;
};

#endif 

