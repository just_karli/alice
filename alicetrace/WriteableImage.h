#pragma once
#ifndef WRITEABLE_IMAGE_H_
#define WRITEABLE_IMAGE_H_
#include <glm/glm.hpp>
#include "Image.h"
#include "../renderlib/Image.h"

class WriteableImage
{
public:
	WriteableImage();
	~WriteableImage() = default;

	ImageBlob image;
	ImageDescription imageDescription;

	void writePixel(unsigned int x, unsigned int y, unsigned char color);
	void writePixel(unsigned int x, unsigned int y, glm::vec3 color);

	void clear();
	void resize(unsigned int width, unsigned int height);

	bool hasChanged() const;
	void resetChanged();
	int getHeight() const;
	int getWidth() const;

private:
	bool m_hasChanged = false;
};

#endif 