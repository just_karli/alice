#version 330 
in vec2 uv;
out vec4 color;

uniform sampler2D inputTexture;

void main() 
{
    color = vec4(uv, 0, 1);
    color = texture(inputTexture, uv);
}