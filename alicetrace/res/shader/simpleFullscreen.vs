#version 330

in vec3 position;
in vec2 texcoord;

uniform mat4 uModel;

out vec2 uv;
void main() 
{
    gl_Position = uModel * vec4(position, 1);
    uv = texcoord;
}