#include "Framebuffer.h"
#include "GL/glew.h"
Framebuffer::~Framebuffer()
{
	m_isBound = false;
	glDeleteFramebuffers(1, &m_framebufferId);
}

bool Framebuffer::create()
{
	if (m_framebufferId == 0)
		glCreateFramebuffers(1, &m_framebufferId);

	return m_framebufferId > 0;
}

bool Framebuffer::bind()
{
	if (!m_isBound)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, m_framebufferId);
		m_isBound = true;
	}

	return m_isBound;
}

bool Framebuffer::unbind()
{
	if(m_isBound)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		m_isBound = false;
	}
	
	return !m_isBound;
}

bool Framebuffer::isFramebufferComplete() const
{
	GLenum framebufferStatus = glCheckNamedFramebufferStatus(m_framebufferId, GL_DRAW_FRAMEBUFFER);

	switch(framebufferStatus)
	{
	case GL_FRAMEBUFFER_UNDEFINED:
		// no window exists?
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT: 
		// check status of each attachment
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
		// attach at least one buffer to the fbo
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
		// check that all attachments enabled via glDrawBuffers exists in fbo
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
		// check that the buffer specified via glReadBuffer exists in fbo
		break;
	case GL_FRAMEBUFFER_UNSUPPORTED:
		// reconsider formats used for attached buffers
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
		// make sure the number of samples for each attachment is the same
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
		// make sure the number of layers for each attachment is the same
		break;
	default:
		break;
	}

	return (framebufferStatus == GL_FRAMEBUFFER_COMPLETE);
}
