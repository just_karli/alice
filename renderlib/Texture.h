#ifndef TEXTURE_H_
#define TEXTURE_H_

#include "Image.h"
#include <GL/glew.h>

class Texture
{
public:
	Texture() = default;

	Texture(const Texture&) = delete;
	Texture& operator=(const Texture&) = delete;

	Texture(Texture&& texture) = default;
	Texture& operator=(Texture&& texture) = default;

	~Texture()
	{
		if (m_textureId > 0)
			glDeleteTextures(1, &m_textureId);

		m_textureId = 0;
	}

	static Texture* loadTexture(const std::vector<unsigned char>& blob, const ImageDescription& imageDesc)
	{
		if (blob.empty())
			return nullptr;

		Texture* texture = new Texture;
		texture->m_width = imageDesc.width;
		texture->m_height = imageDesc.height;

		GLint internalTextureFormat = GL_RGBA;

		GLenum imageFormat = getImageFormat(imageDesc.channels);
		GLenum imageType = getImageGlType(imageDesc.imageType);

		glGenTextures(1, &texture->m_textureId);

		glBindTexture(GL_TEXTURE_2D, texture->getId());

		glTexImage2D(GL_TEXTURE_2D, 0, internalTextureFormat, imageDesc.width, imageDesc.height, 0, imageFormat, imageType, blob.data());

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		if (imageDesc.mipmapEnabled)
		{
			glGenerateMipmap(GL_TEXTURE_2D);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		}

		glBindTexture(GL_TEXTURE_2D, 0);
		return texture;
	}

	static Texture* loadTexture(const Image* image)
	{
		if (image == nullptr)
			return nullptr;

		Texture* texture = nullptr;

		if(image->data != nullptr)
		{
			texture = new Texture;
			texture->m_width = image->width;
			texture->m_height = image->height;

			GLint internalTextureFormat = GL_RGBA;

			GLenum imageFormat = getImageFormat(image->channels);
			GLenum imageType = getImageGlType(image->imageType);

			glGenTextures(1, &texture->m_textureId);

			glBindTexture(GL_TEXTURE_2D, texture->getId());

			glTexImage2D(GL_TEXTURE_2D, 0, internalTextureFormat, image->width, image->height, 0, imageFormat, imageType, image->data->data());
			
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			if (image->mipmapEnabled)
			{
				glGenerateMipmap(GL_TEXTURE_2D);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			}

			glBindTexture(GL_TEXTURE_2D, 0);
		}

		return texture;
	}

	int getWidth() const { return m_width; }
	int getHeight() const { return m_height; }

	GLuint getId() const { return m_textureId; }

	static GLenum getImageGlType(int imageType)
	{
		GLenum imageDatatype = GL_UNSIGNED_BYTE;
		switch(imageType)
		{
		case(1) :	// bitmap
			imageDatatype = GL_UNSIGNED_BYTE;
			break;
		case(2) :	// uint16
			imageDatatype = GL_UNSIGNED_SHORT;
			break;
		case(3) :	// int16
			imageDatatype = GL_SHORT;
			break;
		case(4) :	// uint32
			imageDatatype = GL_UNSIGNED_INT;
			break;
		case(5) :	// int32
			imageDatatype = GL_INT;
			break;
		case(6) :	// float
			imageDatatype = GL_FLOAT;
			break;
		case(7) :	// double
		case(8) :	// 2 x double
			imageDatatype = GL_DOUBLE;
			break;
		case(9) :	// 3x16 bit
		case(10) :	// 4x16 bit
			imageDatatype = GL_UNSIGNED_SHORT;
			break;
		case(11) :	// 3x 32 floating point
		case(12) :	// 4x16 bit
			imageDatatype = GL_FLOAT;
			break;
		default: 
			imageDatatype = GL_UNSIGNED_BYTE;
		};

		return imageDatatype;
	}

	static GLenum getImageFormat(const int channels)
	{
		if (channels == 1)
			return GL_RED;

		if (channels == 3)
			return GL_BGR;

		return GL_RGBA;
	}

	static GLint getTextureFormat(const int channels)
	{
		if (channels == 1)
			return GL_RED;
		if (channels == 2)
			return GL_RG;
		if (channels == 3)
			return GL_RGB;

		return GL_RGBA;
	}

private:
	int m_width = 0;
	int m_height = 1;

	GLuint m_textureId = 0;
};

#endif 
