#ifndef SHADER_H__
#define SHADER_H__

#include <GL/glew.h>

#include <map>
#include <glm/glm.hpp>
#include <vector>

class Texture;

class ShaderProgram
{
public:
	enum class Shadertype
	{
		UNDEFINED,
		VERTEX_SHADER,
		TESSELLATION_CONTROL,
		TESSELLATION_EVALUATION,
		GEOMETRY_SHADER,
		FRAGMENT_SHADER,
		COMPUTE_SHADER
	};

	ShaderProgram() = default;
	~ShaderProgram();

	static bool CheckCompileStatus(GLuint shaderId);
	static bool CheckProgramStatus(GLuint programId, int status);

	static std::string getShadertypeString(Shadertype shadertype);
	static GLuint getGlShadertype(Shadertype shadertype);

	ShaderProgram* loadFromFile(const std::string& path, Shadertype shadertype);
	ShaderProgram* loadVertexShader(const std::string& path);
	ShaderProgram* loadTessellationShader(const std::string& tessControlPath, const std::string& tessEvalPath);
	ShaderProgram* loadGeometryShader(const std::string& path);
	ShaderProgram* loadFragmentShader(const std::string& path);
	ShaderProgram* loadComputeShader(const std::string& path);

	bool compile();
	bool reloadShader();

	void bind();
	void unbind();
	
	void setTexture(const std::string& samplerId, unsigned int textureId, int samplerSlot, unsigned int textureTarget);
	void setTexture(const std::string& samplerId, Texture* texture, int samplerSlot);

	void setUniformi(const std::string& uniformId, const int& iv);
	void setUniformf(const std::string& uniformId, const float& fv);

	void setUniform(const std::string& uniformId, const glm::vec2& vector);
	void setUniform(const std::string& uniformId, const glm::vec3& vector);
	void setUniform(const std::string& uniformId, const glm::vec4& vector);
	void setUniform(const std::string& uniformId, const glm::mat4& matrix);

	void setUniform(const std::string& uniformId, const int* list, size_t count);
	void setUniform(const std::string& uniformId, std::vector<int>& list);

	void setUniform(const std::string& uniformId, const float* list, size_t count);
	void setUniform(const std::string& uniformId, std::vector<float>& list);

	void setUniform(const std::string& uniformId, const glm::vec2* list, size_t count);
	void setUniform(const std::string& uniformId, std::vector<glm::vec2>& list);

	void setUniform(const std::string& uniformId, const glm::vec3* list, size_t count);
	void setUniform(const std::string& uniformId, std::vector<glm::vec3>& list);

	void setUniform(const std::string& uniformId, const glm::vec4* list, size_t count);
	void setUniform(const std::string& uniformId, std::vector<glm::vec4>& list);

	void setUniform(const std::string& uniformId, const glm::mat3* list, size_t count);
	void setUniform(const std::string& uniformId, std::vector<glm::mat3>& list);

	void setUniform(const std::string& uniformId, const glm::mat4* list, size_t count);
	void setUniform(const std::string& uniformId, std::vector<glm::mat4>& list);


	int getId() const { return m_program; }

	const std::string& getInfoText() const { return m_infoText; }
	void setInfoText(const std::string& infoText) { m_infoText = infoText; }

private:
	size_t createShader(const std::string& shaderSource, Shadertype shadertype);
	void attachShader(const size_t shaderId) const;
	int getUniformLocation(const std::string& uniformId);
	void clearUniforms();

	GLuint m_program = 0;
	bool m_isBound = false;

	std::map<std::string, std::string> m_uniforms;
	std::map<std::string, int> m_uniformLocations;

	std::string m_infoText;

	std::string m_vsFilepath;
	std::string m_tsControlFilepath;
	std::string m_tsEvalFilepath;
	std::string m_gsFilepath;
	std::string m_fsFilepath;
	std::string m_csFilepath;
	
	std::string m_vsSource;
	std::string m_tsControlSource;
	std::string m_tsEvalSource;
	std::string m_gsSource;
	std::string m_fsSource;
	std::string m_csSource;
};

#endif
