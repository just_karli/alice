#include "Camera.h"
#include <glm\gtc\matrix_transform.hpp>

void Camera::updateViewMatrix()
{
	m_viewMatrix = glm::lookAtRH(m_position, m_position + m_lookDirection, m_cameraUpDirection);
}

void Camera::updateViewMatrix(const glm::vec3 position, const glm::vec3 forward, const glm::vec3 up)
{
	m_position = position;
	m_lookDirection = glm::normalize(forward);
	m_cameraUpDirection = glm::normalize(up);

	updateViewMatrix();
}

void Camera::setCanvasSize(const int width, const int height)
{
	m_width = width;
	m_height = height;
}

glm::vec2 Camera::getClippingPlanes() const
{
	return glm::vec2(m_znear, m_zfar);
}

glm::vec2 Camera::getCanvasSize() const
{
	return glm::vec2(m_width, m_height);
}

void Camera::setClippingPlanes(const float nearPlane, const float farPlane)
{
	m_znear = nearPlane;
	m_zfar = farPlane;
}

void Camera::setFieldOfView(const float fovx)
{
	m_fovx = fovx;
	m_focalLength = m_width * 0.5f / tan(glm::radians(fovx * 0.5f));
	
	m_fovy = glm::degrees(atan(m_height * 0.5f / m_focalLength) * 2);
}

glm::vec2 Camera::getFov() const
{
	return glm::vec2(m_fovx, m_fovy);
}

void Camera::updateProjection()
{
	// somethings wrong with the glm perspective projection
	m_projection = glm::perspective(glm::radians(m_fovy), m_width / float(m_height), m_znear, m_zfar);
}

void Camera::updateViewProjection()
{
	updateViewMatrix();
	updateProjection();
}

glm::vec3 Camera::getRight() const
{
	glm::vec3 right = glm::cross(m_lookDirection, m_cameraUpDirection);
	right = glm::normalize(right);
	return right;
}

void Camera::setLookDirection(const glm::vec3& lookDirection)
{
	m_lookDirection = lookDirection;
}