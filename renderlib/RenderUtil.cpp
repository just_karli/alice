#include "RenderUtil.h"
#include <iostream>

void RenderUtil::ClearScreen()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

bool RenderUtil::InitGraphics()
{
	glewExperimental = true;
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialize GLEW" << std::endl;
		getchar();
		
		return false;
	}

	return true;
}

std::string RenderUtil::GetOpenGlVersion()
{
	return reinterpret_cast<char const*>(glGetString(GL_VERSION));
}

void RenderUtil::EnableDepthTest()
{
	glEnable(GL_DEPTH_TEST);
}

void RenderUtil::DisableDepthTest()
{
	glDisable(GL_DEPTH_TEST);
}

void RenderUtil::enableDepthWrite()
{
	glDepthMask(GL_TRUE);
}

void RenderUtil::disableDepthWrite()
{
	glDepthMask(GL_FALSE);
}

void RenderUtil::enableWireframe()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

void RenderUtil::disableWireFrame()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void RenderUtil::setClearColor(glm::vec3 color)
{
	glClearColor(color.r, color.b, color.g, 1);
}

void RenderUtil::setViewport(int x, int y, int width, int height)
{
	glViewport(x, y, width, height);
}

void RenderUtil::setViewport(RectF rectangle)
{
	setViewport(static_cast<int>(rectangle.x1), static_cast<int>(rectangle.y1), static_cast<int>(rectangle.GetWidth()), static_cast<int>(rectangle.GetHeight()));
}

RectF RenderUtil::createFittingRectangle(const glm::vec2& textureDimensions, const glm::vec2 windowDimensions)
{
	float textureAspectRatio = textureDimensions.x / textureDimensions.y;
	float windowAspectRatio = windowDimensions.x / windowDimensions.y;

	RectF rectangle;

	if (textureDimensions.x <= windowDimensions.x && textureDimensions.y <= windowDimensions.y)
	{
		rectangle.x1 = windowDimensions.x * 0.5f - textureDimensions.x * 0.5f;
		rectangle.y1 = windowDimensions.y * 0.5f - textureDimensions.y * 0.5f;
		rectangle.x2 = windowDimensions.x * 0.5f + textureDimensions.x * 0.5f;
		rectangle.y2 = windowDimensions.y * 0.5f + textureDimensions.y * 0.5f;
	}
	else
	{
		if (textureAspectRatio > windowAspectRatio)
		{
			rectangle.x1 = 0.0f;
			rectangle.y1 = windowDimensions.y * 0.5f - (windowDimensions.x * 0.5f / textureAspectRatio);
			rectangle.x2 = windowDimensions.x;
			rectangle.y2 = windowDimensions.y * 0.5f + (windowDimensions.x * 0.5f / textureAspectRatio);
		}
		else
		{
			rectangle.x1 = windowDimensions.x * 0.5f - (windowDimensions.y * 0.5f * textureAspectRatio);
			rectangle.y1 = 0.0f;
			rectangle.x2 = windowDimensions.x * 0.5f + (windowDimensions.y * 0.5f * textureAspectRatio);
			rectangle.y2 = windowDimensions.y;
		}
	}

	return rectangle;
}
