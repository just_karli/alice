#ifndef RENDER_UTIL_H__
#define RENDER_UTIL_H__
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <string>
#include <glm/glm.hpp>
#include "RectF.h"

class RenderUtil 
{
public:
	static void ClearScreen();
	static bool InitGraphics();
	static std::string GetOpenGlVersion();

	static void EnableDepthTest();
	static void DisableDepthTest();

	static void enableDepthWrite();
	static void disableDepthWrite();

	static void enableWireframe();
	static void disableWireFrame();

	static void setClearColor(glm::vec3 color);
	static void setViewport(int x, int y, int width, int height);
	static void setViewport(RectF rectangle);

	static RectF createFittingRectangle(const glm::vec2& textureDimensions, const glm::vec2 windowDimensions);

};

#endif 
