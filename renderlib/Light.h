#pragma once
#ifndef LIGHT_H_
#define LIGHT_H_

#include <glm/glm.hpp>
#include "ShaderProgram.h"

struct Light
{
	int type;
	glm::vec3 position;
	glm::vec3 direction;

	glm::vec3 ambientColor = glm::vec3(0.2f);
	glm::vec3 diffuseColor = glm::vec3(0.5f);
	glm::vec3 specularColor = glm::vec3(1.0f);

	float constantAttenuation = 1.0f;
	float linearAttenuation = 0.09f;
	float quadraticAttenuation = 0.032f;

	float cutOffDegrees = 12.5f;
	float outerCutoffDegrees = 15.0f;

	void setUniform(ShaderProgram& shader, const std::string& memberName) const;
};

#endif 