#ifndef TIMER_H__
#define TIMER_H__

#include <chrono>
#include <ostream>

class Timer 
{
public:
	static const long long SECONDS = 1000;
	explicit Timer(bool run = false)
	{
		if (run)
			Reset();
	}

	void Reset() 
	{
		m_start = std::chrono::high_resolution_clock::now();
	}

	std::chrono::milliseconds Elapsed() const 
	{
		return std::chrono::duration_cast<std::chrono::milliseconds>(
			std::chrono::high_resolution_clock::now() - m_start
		);
	}

	static std::chrono::high_resolution_clock::time_point CurrentTime()
	{
		return std::chrono::high_resolution_clock::now();
	}

	template <typename T, typename Traits>
	friend std::basic_ostream<T, Traits>& operator <<(std::basic_ostream<T, Traits>& out, const Timer& timer)
	{
		return out << timer.Elapsed().count();
	}

private:
	std::chrono::high_resolution_clock::time_point m_start;
};

#endif
