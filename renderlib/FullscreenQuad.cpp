#include "FullscreenQuad.h"
#include "MeshUtilities.h"

bool FullscreenQuad::initialize()
{
	m_buffer = std::make_unique<Drawable>();
	const std::vector<glm::vec3> quad = MeshUtilities::generateQuadVertices();
	const std::vector<glm::vec2> uvs = MeshUtilities::generateQuadUvs();

	m_buffer->AddVertices(quad);
	m_buffer->AddTextureCoordinates(uvs);
	return true;
}

void FullscreenQuad::release()
{
	m_buffer = nullptr;
}

void FullscreenQuad::draw(ShaderProgram* program) const
{
	if (program != nullptr && program->getId() > 0)
	{
		program->setUniform("uModel", glm::mat4(1));
		m_buffer->Draw(*program);
	}
}
