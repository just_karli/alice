#ifndef IMAGELOADER_H__
#define IMAGELOADER_H__

#include "Image.h"

class ImageLoader
{
public:
	static ImageLoader& GetInstance()
	{
		static ImageLoader instance;
		return instance;
	}

	ImageLoader(ImageLoader const&) = delete;
	void operator =(ImageLoader const&) = delete;

	Image* loadImage(const std::string& imagePath) const;
private:
	ImageLoader();
	~ImageLoader();
};

#endif
