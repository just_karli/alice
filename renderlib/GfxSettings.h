#ifndef GFX_SETTINGS_H__
#define GFX_SETTINGS_H__
#include <string>

struct GfxSettings
{
	int windowWidth = 1024;
	int windowHeight = 768;
	std::string windowTitle = "Karligine";

	bool fullscreenWindow = false;
	bool visibleWindow = true;
	bool windowOnTop = false;

	int msaaCount = 4;

	int depthBits = 24;
	int stencilBits = 8;

	bool debugContext = true;

	int refreshRate = 0;

	std::string iconPath;
};

#endif 
