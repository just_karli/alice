#pragma once
#ifndef BOUNDING_BOX_H_
#define BOUNDING_BOX_H_

#include <glm/glm.hpp>

struct BoundingBox
{
	glm::vec4 minimum;
	glm::vec4 maximum;
};

#endif 