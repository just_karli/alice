#pragma once
#ifndef DATA_H_
#define DATA_H_

#include <atomic>
#include <functional>
#include <mutex>
#include <memory>
#include <type_traits>
#include <vector>

namespace tachyon
{
	// https://jguegant.github.io/blogs/tech/thread-safe-multi-type-map.html
	struct DefaultKey;
	template <class Type, class Key = DefaultKey> class Data;

	template <class Type, class Key>
	class Watcher
	{
	public:
		Watcher(Data<Type, Key>& d) : m_data(d), m_hasBeenChanged(false)
		{
		}

		// avoid copy & assignment
		Watcher(const Watcher&) = delete;
		Watcher& operator=(const Watcher&) = delete;

		bool hasBeenChanged() const
		{
			return m_hasBeenChanged;
		}

		void triggerChanges()
		{
			m_hasBeenChanged = true;
		}

		std::shared_ptr<Type> get()
		{
			m_hasBeenChanged = false;
			return m_data.doGet();
		}

	private:
		Data<Type, Key> m_data;
		std::atomic_bool m_hasBeenChanged;
	};

	template<class Type, class Key>
	class Data
	{
	public:
		using WatcherType = Watcher<Type, Key>;
		using WatcherTypeUptr = std::unique_ptr<WatcherType, std::function<void(WatcherType*)>>;

		std::shared_ptr<Type> doGet() const
		{
			return std::atomic_load(&m_value);
		}

		void doSet(const std::shared_ptr<Type>& value)
		{
			std::atomic_exchange(&m_value, value);
			signal();
		}

		WatcherTypeUptr doGetWatcher()
		{
			// create unique watcher with custom deleter and store the value ptr
			WatcherTypeUptr watcher(new WatcherType(*this),
				[this](WatcherType* toBeDelete)
			{
				this->unregisterWatcher(toBeDelete);
			});

			registerWatcher(watcher.get());

			return watcher;
		}

	private:
		void registerWatcher(WatcherType* newWatcher)
		{
			std::lock_guard<std::mutex> lg(m_watchersMutex);
			m_watchers.push_back(newWatcher);
		}

		void unregisterWatcher(WatcherType* toBeDelete)
		{
			std::lock_guard<std::mutex> lg(m_watchersMutex);
			m_watchers.erase(std::remove(m_watchers.begin(), m_watchers.end(), toBeDelete), m_watchers.end());

			delete toBeDelete;
		}

		void signal()
		{
			std::lock_guard<std::mutex> lg(m_watchersMutex);
			for (auto watcher : m_watchers)
				watcher->triggerChanges();
		}

		std::vector<Watcher<Type, Key>*> m_watchers;
		std::mutex m_watchersMutex;

		std::shared_ptr<Type> m_value;
	};

	template<class... Datas>
	class DataSources : Datas...
	{
	public:
		template <class Type, class Key = DefaultKey>
		std::shared_ptr<Type> get()
		{
			static_assert(std::is_base_of<Data<Type, Key>, DataSources<Datas...>>::value,
				"Please ensure that this type or this key exists in this repository!");
			return Data<Type, Key>::doGet();
		}

		template<class Type, class Key = DefaultKey>
		void set(const std::shared_ptr<Type>& value)
		{
			static_assert(std::is_base_of<Data<Type, Key>, DataSources<Datas...>>::value,
				"Please ensure that this type or this key exists in this repository!");

			Data<Type, Key>::doSet(value);
		}

		template<class Type, class Key = DefaultKey, class ...Args>
		void emplace(Args&&... args)
		{
			static_assert(std::is_base_of<Data<Type, Key>, DataSources<Datas...>>::value,
				"Please ensure that this type or this key exists in this repository!");

			return Data<Type, Key>::doSet(std::make_shared<Type>(std::forward<Args>(args)...));
		}

		template<class Type, class Key = DefaultKey>
		typename Data<Type, Key>::WatcherTypeUptr getWatcher()
		{
			static_assert(std::is_base_of<Data<Type, Key>, DataSources<Datas...>>::value,
				"Please ensure that this type or this key exists in this repository!");

			return Data<Type, Key>::doGetWatcher();
		}
	};

}

#endif // DATA_H_