#ifndef CAMERA_H__
#define CAMERA_H__

#include <ostream>
#include <glm/glm.hpp>

class Camera
{
public:
	void updateViewMatrix(const glm::vec3 position, const glm::vec3 forward, const glm::vec3 up);
	
	void updateViewMatrix();
	void updateProjection();
	void updateViewProjection();

	void setCanvasSize(const int width, const int height);
	glm::vec2 getCanvasSize() const;

	void setClippingPlanes(const float nearPlane, const float farPlane);
	glm::vec2 getClippingPlanes() const;
	
	void setFieldOfView(const float fovx);
	glm::vec2 getFov() const;
	
	glm::mat4 getProjection() const { return m_projection; }
	glm::mat4 getViewMatrix() const { return m_viewMatrix; }

	glm::vec3 getPosition() const { return m_position; }
	void setPosition(const glm::vec3& position) { m_position = position; }

	glm::vec3 getRight() const;
	glm::vec3 getLookDirection() const { return m_lookDirection; }
	void setLookDirection(const glm::vec3& lookDirection);
	glm::vec3 getCameraUpDirection() const { return m_cameraUpDirection; }
	void setCameraUpDirection(const glm::vec3& upDirection) { m_cameraUpDirection = upDirection; }

	template <typename T, typename Traits>
	friend std::basic_ostream<T, Traits>& operator <<(std::basic_ostream<T, Traits>& out, const Camera& camera)
	{
		const glm::vec3 position = camera.getPosition();
		return out << "Camera Position: (" << position.x << ", " << position.y << ", " << position.z << ") " <<
			"look at: (" << camera.m_lookDirection.x << ", " << camera.m_lookDirection.y << ", " << camera.m_lookDirection.z << ")";
	}

private:
	float m_znear = 0.1f;
	float m_zfar = 5000.0f;
	int m_width = 1024;
	int m_height = 768;

	float m_fovx = 90.0f;
	float m_fovy = 0.0f;
	float m_focalLength = 0.0f;

	glm::mat4 m_projection;

	glm::vec3 m_position = glm::vec3(0, 0, 5);
	glm::vec3 m_lookDirection = glm::vec3(0, 0, -1.0f);
	glm::vec3 m_cameraUpDirection = glm::vec3(0, 1, 0);

	glm::mat4 m_viewMatrix;
};

#endif
