#pragma once
#ifndef LIGHT_TYPE_H_
#define LIGHT_TYPE_H_

enum class LightType : int	
{
	SPOTLIGHT = 0,
	POINTLIGHT,
	DIRECTIONALLIGHT
};

#endif 
