#ifndef RECTANGLEF_H__
#define RECTANGLEF_H__

#include <algorithm>

struct RectF
{
	float x1;
	float y1;
	float x2;
	float y2;

	float GetWidth() const
	{
		return std::max(x1, x2) - std::min(x1, x2);
	}

	float GetHeight() const
	{
		return std::max(y1, y2) - std::min(y1, y2);
	}
};

#endif 
