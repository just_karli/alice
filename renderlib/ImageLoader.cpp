#include "ImageLoader.h"

#include <iostream>
#include <memory>
#include "FreeImage.h"
#include <vector>

ImageLoader::ImageLoader()
{
	FreeImage_Initialise();
}

ImageLoader::~ImageLoader()
{
	FreeImage_DeInitialise();
}

Image* ImageLoader::loadImage(const std::string& imagePath) const
{
	// check if is already cached..
	const char* filename = imagePath.c_str();

	// collect image format infos
	FREE_IMAGE_FORMAT format = FreeImage_GetFileType(filename, 0);
	if (format == -1)
	{
		std::cout << "Could not find image: " << filename << std::endl;
		getchar();
		return nullptr;
	}

	if (format == FIF_UNKNOWN)
	{
		// check filename extension support
		format = FreeImage_GetFIFFromFilename(filename);
		if (!FreeImage_FIFSupportsReading(format))
		{
			std::cout << "Detected image " << filename << " can not be read!" << std::endl;
			getchar();
			return nullptr;
		}
	}

	// load image & get image infos
	FIBITMAP* bitmap = FreeImage_Load(format, filename);

	const int bitsPerPixel = FreeImage_GetBPP(bitmap);
	const int imageWidth = FreeImage_GetWidth(bitmap);
	const int imageHeight = FreeImage_GetHeight(bitmap);
	const FREE_IMAGE_TYPE imageType = FreeImage_GetImageType(bitmap);

	int channels = 0;

	// get channel info
	if (imageType == FIT_BITMAP)
	{
		// default bitmap 
		// 8 bit per channel 
		channels = static_cast<int>(ceil(bitsPerPixel / 8));
	}
	else if (
				imageType == FIT_INT16 || imageType == FIT_UINT16 ||
				imageType == FIT_INT32 || imageType == FIT_UINT32 ||
				imageType == FIT_FLOAT || imageType == FIT_DOUBLE
			)
	{
		channels = 1;
	}
	else if (imageType == FIT_COMPLEX)
	{
		channels = 2;
	}
	else if (imageType == FIT_RGB16 || imageType == FIT_RGBF)
	{
		channels = 3;
	}
	else if (imageType == FIT_RGBA16 || imageType == FIT_RGBAF)
	{
		channels = 4;
	}

	Image* image = new Image();
	image->filepath = imagePath;
	image->width = imageWidth;
	image->height = imageHeight;
	image->bitsPerPixel = bitsPerPixel;
	image->channels = channels;
	image->imageType = imageType;

	const int pitch = FreeImage_GetPitch(bitmap);
	int size = pitch * imageHeight;
	
	unsigned char* rawImagePtr = FreeImage_GetBits(bitmap);
	std::vector<unsigned char> imageData;
	imageData.assign(rawImagePtr, rawImagePtr + size);

	image->data = std::make_unique<std::vector<unsigned char>>(std::move(imageData));

	// remove image from memory
	bitmap = nullptr;

	return image;
}
