/**
 * Flythrough camera manipulator implementation based on a implementation of Nicolas Guillemot 
 * https://github.com/nlguillemot/flythrough_camera
 */

#pragma once
#ifndef FLY_THROUGH_MANIPULATOR_H_
#define FLY_THROUGH_MANIPULATOR_H_

#include <glm/glm.hpp>

class InputHandler;
class Camera;

class FlythroughManipulator
{
public:
	void update(Camera& camera,
		float deltaSeconds,
		InputHandler* input
	) const;

	float movementSpeed = 0.025f;

private:
	float m_degreesPerCursorMove = 0.125f;
	float m_maxPitchRotationDegrees = 80.0f;
};

#endif