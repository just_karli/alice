#pragma once
#ifndef FRAMEBUFFER_H_
#define FRAMEBUFFER_H_

class Framebuffer
{
public:
	Framebuffer() = default;
	~Framebuffer();

	bool create();
	bool bind();
	bool unbind();

	bool isFramebufferComplete() const;

private: 

	bool m_isBound = false;
	unsigned int m_framebufferId = 0;
};

#endif 