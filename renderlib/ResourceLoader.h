#ifndef RESOURCELOADER_H__
#define RESOURCELOADER_H__

#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <regex>
#include <map>

class ResourceLoader
{
public:
	static std::string loadShader(std::string filename)
	{
		std::string content;
		std::ifstream stream(filename, std::ios::in);

		if(stream.is_open())
		{
			std::string line;
			while (!stream.eof())
			{
				std::getline(stream, line);

				content.append(line + "\n");
			}

			stream.close();
		}
		else
		{
			std::cout << "impossible to open " << filename << std::endl;
			getchar();
		}

		return content;
	}

	static std::string processIncludes(const std::string& shaderCode)
	{
		std::string shader = shaderCode;
		const std::string includeToken = "#include \"";
		const std::string endIncludeToken = "\"";
		size_t includeTokenIdx = shader.find(includeToken);

		while (includeTokenIdx != std::string::npos)
		{
			const size_t fileIncludePosition = includeTokenIdx + includeToken.length();
			const size_t endTokenIdx = shader.find(endIncludeToken, fileIncludePosition);
			std::string includeFile = shader.substr(fileIncludePosition, endTokenIdx - fileIncludePosition);

			if (!includeFile.empty())
			{
				std::string includeContent = loadShader(includeFile);
				std::string replaceText = includeToken + includeFile + endIncludeToken;

				replace(shader, replaceText, includeContent);
			}

			includeTokenIdx = shader.find(includeToken, includeTokenIdx + 1);
		}
		
		return shader;
	}

	static bool replace(std::string& content, const std::string& replace, const std::string& replacement)
	{
		size_t startPosition = content.find(replace);
		if (startPosition == std::string::npos)
			return false;

		content.replace(startPosition, replace.length(), replacement);
		return true;
	}

	static std::string trim(const std::string& str)
	{
		if (str.empty())
			return str;

		size_t first = str.find_first_not_of(' ');
		size_t last = str.find_last_not_of(' ');
		return str.substr(first, (last - first + 1));
	}

	static std::map<std::string, std::string> collectUniforms(const std::string& shader)
	{
		// name, type
		std::map<std::string, std::string> uniforms;

		const std::string token = "uniform";
		const std::string endToken = ";";
		size_t tokenIdx = shader.find(token);
		while(tokenIdx != std::string::npos)
		{
			size_t uniformStartIdx = tokenIdx + token.length() + 1;
			size_t tokenEndIdx = shader.find(endToken, tokenIdx);
			std::string uniformDefinition = shader.substr(uniformStartIdx, tokenEndIdx - uniformStartIdx);

			// split by " " 
			std::istringstream iss(uniformDefinition);
			std::vector<std::string> splittedUniformDefinitions{ std::istream_iterator<std::string>{iss}, std::istream_iterator<std::string>{} };

			if (splittedUniformDefinitions.size() >= 2)
				uniforms.insert(std::make_pair(splittedUniformDefinitions[1], splittedUniformDefinitions[0]));

			tokenIdx = shader.find(token, tokenIdx + 1);
		}

		return uniforms;
	}
};

#endif
