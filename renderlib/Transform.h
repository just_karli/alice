#ifndef TRANSFORM_H__
#define TRANSFORM_H__

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
class Transform
{
public:
	Transform() : m_position(glm::vec3(0.0f)), m_rotation(glm::vec3(0.0f)), m_scale(1.0f) { }
	~Transform() = default;

	glm::vec3 getPosition() const { return m_position; }
	void setPosition(glm::vec3& v) { m_position = v; }

	glm::vec3 getRotation() const { return m_rotation; }
	void setRotation(float x, float y, float z) { m_rotation = glm::vec3(x, y, z); }

	glm::vec3 getScale() const { return m_scale; } 
	void setScale(float x, float y, float z) { m_scale = glm::vec3(x, y, z); }

	glm::mat4 getTransformation() const
	{
		glm::mat4 translation = glm::translate(glm::mat4(1.0f), m_position);
		glm::mat4 rotation = glm::rotate(glm::mat4(1.0f), glm::radians(m_rotation.x), glm::vec3(1.0f, 0.0f, 0.0f)) *
			glm::rotate(glm::mat4(1.0f), glm::radians(m_rotation.y), glm::vec3(0.0f, 1.0f, 0.0f)) * 
			glm::rotate(glm::mat4(1.0f), glm::radians(m_rotation.z), glm::vec3(0.0f, 0.0f, 1.0f));

		glm::mat4 scale = glm::scale(glm::mat4(1.0f), m_scale);
		
		glm::mat4 model = translation * rotation * scale;
		return model;
	}

private:
	glm::vec3 m_position;
	glm::vec3 m_rotation;
	glm::vec3 m_scale;
};

#endif 
