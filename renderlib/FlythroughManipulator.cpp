#include "FlythroughManipulator.h"

#include "Camera.h"
#include "InputHandler.h"
#include <glm/gtc/matrix_transform.inl>
#include <GLFW/glfw3.h>

void FlythroughManipulator::update(Camera& camera, float deltaSeconds, InputHandler* input) const
{
	glm::vec3 currentPosition = camera.getPosition();
	glm::vec3 look = camera.getLookDirection();
	glm::vec3 up = camera.getCameraUpDirection();

	bool rightKeyPressed = input->isKeyPressed(GLFW_KEY_D);
	bool leftKeyPressed = input->isKeyPressed(GLFW_KEY_A);
	bool forwardKeyPressed = input->isKeyPressed(GLFW_KEY_W);
	bool backwardKeyPressed = input->isKeyPressed(GLFW_KEY_S);
	bool upwardKeyPressed = input->isKeyPressed(GLFW_KEY_PAGE_UP);
	bool downwardKeyPressed = input->isKeyPressed(GLFW_KEY_PAGE_DOWN);

	// generate new right vector from look and up vector
	glm::vec3 right = glm::normalize(glm::cross(glm::normalize(look), glm::normalize(up)));
	right = glm::normalize(right);

	// apply xz (wasd) movement
	float x_direction = (rightKeyPressed ? 1.0f : 0.0f) - (leftKeyPressed ? 1.0f : 0.0f);
	float z_direction = (forwardKeyPressed ? 1.0f : 0.0f) - (backwardKeyPressed ? 1.0f : 0.0f);

	glm::vec3 xz_movement = right * x_direction + look * z_direction;

	glm::vec3 position = currentPosition + xz_movement * movementSpeed * deltaSeconds;
	
	// apply up / down movement
	float y_distance = (upwardKeyPressed ? 1.0f : 0.0f) - (downwardKeyPressed ? 1.0f : 0.0f);
	glm::vec3 y_movement = glm::normalize(up) * y_distance;

	position = position + y_movement * movementSpeed * deltaSeconds;

	// generate new up vector from right and look vector 
	glm::vec3 upward = glm::normalize(glm::cross(glm::normalize(right), glm::normalize(look)));

	bool isMouseLookEnabled = input->isKeyPressed(GLFW_KEY_LEFT_ALT) && input->isMousePressed(GLFW_MOUSE_BUTTON_LEFT);
	glm::vec2 defaultMousePosition = camera.getCanvasSize() * 0.5f;

	// mouse delta is only changed if mouse look is enabled & mouse has been moved
	glm::vec2 mouseDelta = glm::vec2(0);

	if(isMouseLookEnabled)
	{
		bool isInitialClick = !input->isCursorHidden();
		if(isInitialClick)
		{
			// set mouse at initial position
			input->setCursorPosition(defaultMousePosition);
			input->hideCursor();
		}
		else
		{
			// get and set new mouse position	
			glm::vec2 mouseMove = input->getCursorPosition();
			input->setCursorPosition(defaultMousePosition);
			mouseDelta = defaultMousePosition - mouseMove;
			mouseDelta = mouseDelta * glm::vec2(-1.0f, 1.0f);
		}
	}
	else
	{
		// enable mouse again
		if(input->isCursorHidden())
		{
			input->showCursor();
		}
	}

	// apply yaw rotation (Y-Axis)
	glm::vec3 newForwardDir = look;
	if(mouseDelta.x != 0)
	{
		float yawDegrees = -mouseDelta.x * m_degreesPerCursorMove;
		float yawRadians = glm::radians(yawDegrees);

		glm::mat4 yawRotation = glm::rotate(glm::mat4(1), yawRadians, upward);
		newForwardDir = glm::normalize(glm::mat3(yawRotation) * look);
 	}

	if(mouseDelta.y != 0)
	{
		float radiansToUp = acosf(glm::dot(look, up));
		float radiansToDown = acosf(glm::dot(look, -up));

		float degreesToUp = glm::degrees(radiansToUp);
		float degreesToDown = glm::degrees(radiansToDown);

		float maxPitchDegrees = degreesToUp - (90.0f - m_maxPitchRotationDegrees);
		maxPitchDegrees = glm::max(maxPitchDegrees, 0.0f);

		float minPitchDegrees = degreesToDown - (90.0f - m_maxPitchRotationDegrees);
		minPitchDegrees = glm::max(minPitchDegrees, 0.0f);

		// rotation here is counter-clockwise because sin/cos are counter-clockwise
		float pitch_degrees = mouseDelta.y * m_degreesPerCursorMove;

		if (pitch_degrees < 0.0f && pitch_degrees > maxPitchDegrees)
			pitch_degrees = maxPitchDegrees;

		if (pitch_degrees < 0.0f && -pitch_degrees > minPitchDegrees)
			pitch_degrees = -minPitchDegrees;

		float pitchRadians = glm::radians(pitch_degrees);

		glm::mat4 pitchRotation = glm::rotate(glm::mat4(1), pitchRadians, right);
		newForwardDir = glm::normalize(glm::mat3(pitchRotation) * newForwardDir);
	}

	camera.setPosition(position);
	camera.setLookDirection(newForwardDir);
	camera.updateViewMatrix();
}
