#ifndef IMAGE_H_
#define IMAGE_H_
#include <string>
#include <memory>
#include <vector>

struct ImageDescription
{
	std::string filepath;
	int width;
	int height;

	//! a pixel consist of #channels * data type
	int bitsPerPixel;
	int channels;

	int imageType;
	bool mipmapEnabled = false;
};

struct Image
{
	std::string filepath;
	int width;
	int height;
	
	//! a pixel consist of #channels * data type
	int bitsPerPixel; 
	int channels;

	int imageType;
	bool mipmapEnabled = false;
	
	std::unique_ptr<std::vector<unsigned char>> data = nullptr;
};

#endif 
