#ifndef ASSIMP_LOADER_H__
#define ASSIMP_LOADER_H__

#include "Drawable.h"

class AssimpLoader
{
public:
	static Drawable* loadObject(const std::string& file);
};

#endif 
