#pragma once
#ifndef FULLSCREEN_QUAD_H_
#define FULLSCREEN_QUAD_H_
#include <memory>
#include "Drawable.h"

class FullscreenQuad
{
public:
	bool initialize();
	void release();

	void draw(ShaderProgram* program) const;
private:
	std::unique_ptr<Drawable> m_buffer;
};

#endif 
