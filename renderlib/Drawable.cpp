#include "Drawable.h"
#include <gl/glew.h>

Drawable::Drawable() : m_ibuffer(0), m_vertexCount(0), m_stride(0), m_size(0), m_indicesCount(0)
{
}

Drawable::~Drawable()
{
	glDeleteBuffers(1, &m_vbuffer);
	glDeleteBuffers(1, &m_uvBuffer);
	glDeleteBuffers(1, &m_ibuffer);
}

void Drawable::AddVertices(glm::vec3* vertices, size_t count)
{
	if (m_vbuffer != 0)
		glDeleteBuffers(1, &m_vbuffer);
	glGenBuffers(1, &m_vbuffer);

	m_stride = sizeof(glm::vec3);
	m_vertexCount = count;
	m_size = m_stride * m_vertexCount;

	glBindBuffer(GL_ARRAY_BUFFER, m_vbuffer);
	glBufferData(GL_ARRAY_BUFFER, m_size, vertices, GL_STATIC_DRAW);
}

void Drawable::AddVertices(std::vector<glm::vec3> vertices)
{
	AddVertices(vertices.data(), vertices.size());
}

void Drawable::AddNormals(glm::vec3* normals, size_t count)
{
	if (m_nBuffer != 0)
		glDeleteBuffers(1, &m_nBuffer);
	glGenBuffers(1, &m_nBuffer);

	glBindBuffer(GL_ARRAY_BUFFER, m_nBuffer);
	glBufferData(GL_ARRAY_BUFFER, count * sizeof(glm::vec3), normals, GL_STATIC_DRAW);
}

void Drawable::AddNormals(std::vector<glm::vec3> normals)
{
	AddNormals(normals.data(), normals.size());
}

void Drawable::AddTextureCoordinates(glm::vec2* uvs, size_t count)
{
	if (m_uvBuffer != 0)
		glDeleteBuffers(1, &m_uvBuffer);
	
	glGenBuffers(1, &m_uvBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_uvBuffer);
	glBufferData(GL_ARRAY_BUFFER, count * sizeof(glm::vec2), uvs, GL_STATIC_DRAW);
}

void Drawable::AddTextureCoordinates(std::vector<glm::vec2> texcoords)
{
	AddTextureCoordinates(texcoords.data(), texcoords.size());
}

void Drawable::AddIndices(unsigned int* indices, size_t count)
{
	if (m_ibuffer != 0)
		glDeleteBuffers(1, &m_ibuffer);

	glGenBuffers(1, &m_ibuffer);

	m_indicesCount = count;

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned int), indices, GL_STATIC_DRAW);
}

void Drawable::AddIndices(std::vector<unsigned> indices)
{
	AddIndices(indices.data(), indices.size());
}

// todo checkout if i need to do this? or if i just can use enable vertex attributes?
void Drawable::LinkProgram(const ShaderProgram& shader)
{
	if (shader.getId() == 0)
		return;

	m_positionAttributeLocation = glGetAttribLocation(shader.getId(), "position");
	m_uvAttributeLocation = glGetAttribLocation(shader.getId(), "texcoord");
	m_normalAttributeLocation = glGetAttribLocation(shader.getId(), "normal");
}

// todo checkout if i need to do this? or if i just can use enable vertex attributes?
void Drawable::Draw(const ShaderProgram& shader)
{
	LinkProgram(shader);

	glEnableVertexAttribArray(m_positionAttributeLocation);	// slot 0 is always the vertex
	glBindBuffer(GL_ARRAY_BUFFER, m_vbuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0); // stride vertex.size * 4 size of one individual vertex

	if(m_uvBuffer > 0 && m_uvAttributeLocation > 0)
	{
		glEnableVertexAttribArray(m_uvAttributeLocation);
		glBindBuffer(GL_ARRAY_BUFFER, m_uvBuffer);
		glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, (void*)0);
	}

	if(m_nBuffer > 0 && m_normalAttributeLocation > 0)
	{
		glEnableVertexAttribArray(m_normalAttributeLocation);
		glBindBuffer(GL_ARRAY_BUFFER, m_nBuffer);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}
	
	if(m_ibuffer == 0)
		glDrawArrays(GL_TRIANGLES, 0, static_cast<GLsizei>(m_vertexCount));
	else
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibuffer);
		glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(m_indicesCount), GL_UNSIGNED_INT, (void*)0);
	}

	if (m_normalAttributeLocation > 0)
		glDisableVertexAttribArray(m_normalAttributeLocation);

	if (m_uvAttributeLocation > 0)
		glDisableVertexAttribArray(m_uvAttributeLocation);

	glDisableVertexAttribArray(m_positionAttributeLocation);
}
