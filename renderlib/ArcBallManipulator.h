/**
* Flythrough camera manipulator implementation based on a implementation of Nicolas Guillemot
* https://github.com/nlguillemot/arcball_camera/blob/master/arcball_camera.h
*/

#pragma once
#ifndef ARC_BALL_MANIPULATOR_H_
#define ARC_BALL_MANIPULATOR_H_

#include <glm/glm.hpp>
class Camera;
class InputHandler;

class ArcBallManipulator
{
public:
	glm::mat4 update(Camera& camera, InputHandler* inputHandler, float delta, bool rightHanded);

private:
	float m_zoomPerTick = 0.1f;
	float m_panSpeed = 0.5f;
	float m_rotationMultiplier = 3.0f;

	glm::vec2 m_previousCursorPosition;
};

#endif 