#pragma once
#ifndef GL_ERROR_H_
#define GL_ERROR_H_
enum class SourceError
{
	API, 
	WindowSystem, 
	ShaderCompiler, 
	ThirdParty, 
	Application, 
	Other
};

enum class TypeError
{
	Error, 
	DeprecatedBehaviour, 
	UndefinedBehaviour, 
	Portability, 
	Performance, 
	Marker, 
	PushGroup,
	PopGroup, 
	Other
};

enum class SeverityLevel
{
	High,
	Medium, 
	Low, 
	Notification
};

struct GlError
{
	int ErrorId;
	SourceError Source;
	TypeError Type;
	SeverityLevel Severity;
};
#endif