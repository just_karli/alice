#ifndef MESH_UTILS_H__
#define MESH_UTILS_H__
#include <glm/glm.hpp>
#include <vector>

class MeshUtilities
{
public:
	static std::vector<glm::vec3> generateQuadVertices()
	{
		std::vector<glm::vec3> data =
		{
			glm::vec3(-1.0f, 1.0f, 0.0f),	// TL 
			glm::vec3(-1.0f, -1.0f, 0.0f),	// BL
			glm::vec3(1.0f, -1.0f, 0.0f),	// BR
			glm::vec3(1.0f, -1.0f, 0.0f),	// BR
			glm::vec3(1.0f, 1.0f, 0.0f),	// TR
			glm::vec3(-1.0f, 1.0f, 0.0f)	// TL
		};

		return data;
	}

	static std::vector<glm::vec2> generateQuadUvs()
	{
		std::vector<glm::vec2> data =
		{
			glm::vec2(0, 1),	// TL
			glm::vec2(0, 0),	// BL
			glm::vec2(1, 0),	// BR
			glm::vec2(1, 0),	// BR
			glm::vec2(1, 1),	// TR
			glm::vec2(0, 1)		// TL
		};

		return data;
	}
};

#endif