#include "Light.h"

void Light::setUniform(ShaderProgram& shader, const std::string& memberName) const
{

	shader.setUniformi(memberName + ".type", static_cast<int>(type));
	shader.setUniform(memberName + ".position", position);
	shader.setUniform(memberName + ".direction", direction);

	shader.setUniform(memberName + ".ambient", ambientColor);
	shader.setUniform(memberName + ".diffuse", diffuseColor);
	shader.setUniform(memberName + ".specular", specularColor);
	
	shader.setUniformf(memberName + ".constant", constantAttenuation);
	shader.setUniformf(memberName + ".linear", linearAttenuation);
	shader.setUniformf(memberName + ".quadratic", quadraticAttenuation);

	shader.setUniformf(memberName + ".cutOff", std::cos(glm::radians(cutOffDegrees)));
	shader.setUniformf(memberName + ".outerCutoff", std::cos(glm::radians(outerCutoffDegrees)));
}
