#include "ShaderProgram.h"

#include <vector>
#include "ResourceLoader.h"
#include "Texture.h"
#include <glm/gtc/type_ptr.hpp>

ShaderProgram::~ShaderProgram()
{
	if (m_program > 0)
	{
		glDeleteProgram(m_program);
		m_program = 0;
	}
}

bool ShaderProgram::CheckCompileStatus(GLuint shaderId)
{
	GLint result = GL_FALSE;
	int infologLength = 0;

	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &result);
	glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &infologLength);

	if (!result)
	{
		std::vector<char> shaderError(infologLength + 1);
		glGetShaderInfoLog(shaderId, infologLength, nullptr, &shaderError[0]);
		const std::string log(shaderError.begin(), shaderError.end());
		std::cout << "[Shader]: Error compiling shader - " << log << std::endl;
		getchar();

		return false;
	}

	return true;
}

bool ShaderProgram::CheckProgramStatus(GLuint programId, int status)
{
	GLint result = GL_FALSE;
	int infologLength = 0;

	glGetProgramiv(programId, status, &result);
	glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &infologLength);

	if (!result)
	{
		std::vector<char> shaderError(infologLength + 1);
		glGetProgramInfoLog(programId, infologLength, nullptr, &shaderError[0]);
		std::string log(shaderError.begin(), shaderError.end());
		std::cout << "[Shader]: Error attaching/validating shader - " << log << std::endl;
		getchar();

		return false;
	}

	return true;
}

std::string ShaderProgram::getShadertypeString(Shadertype shadertype)
{
	switch (shadertype)
	{
	case Shadertype::VERTEX_SHADER:
		return "Vertex Shader";
	case Shadertype::TESSELLATION_CONTROL:
		return "Tessellation Control Shader";
	case Shadertype::TESSELLATION_EVALUATION:
		return "Tessellation Evaluation Shader";
	case Shadertype::GEOMETRY_SHADER:
		return "Geometry Shader";
	case Shadertype::FRAGMENT_SHADER:
		return "Fragment Shader";
	case Shadertype::COMPUTE_SHADER:
		return "Compute Shader";
	default:
		return "Undefined Shader";
	}
}

GLuint ShaderProgram::getGlShadertype(Shadertype shadertype)
{
	switch (shadertype)
	{
	case Shadertype::VERTEX_SHADER:
		return GL_VERTEX_SHADER;
	case Shadertype::TESSELLATION_CONTROL:
		return GL_TESS_CONTROL_SHADER;
	case Shadertype::TESSELLATION_EVALUATION:
		return GL_TESS_EVALUATION_SHADER;
	case Shadertype::GEOMETRY_SHADER:
		return GL_GEOMETRY_SHADER;
	case Shadertype::FRAGMENT_SHADER:
		return GL_FRAGMENT_SHADER;
	case Shadertype::COMPUTE_SHADER:
		return GL_COMPUTE_SHADER;
	default:
		return GL_NONE;
	}
}

ShaderProgram* ShaderProgram::loadFromFile(const std::string& path, Shadertype shadertype)
{
	std::string shaderSrc = ResourceLoader::loadShader(path);
	if (shaderSrc.empty())
	{
		std::cout << "[Shader]: Error loading src from: " << path;
		return this;
	}

	shaderSrc = ResourceLoader::processIncludes(shaderSrc);

	if (m_program <= 0)
		m_program = glCreateProgram();

	const size_t shaderId = createShader(shaderSrc, shadertype);
	if (shaderId > 0)
	{
		attachShader(shaderId);

		switch (shadertype)
		{
		case Shadertype::VERTEX_SHADER:
			m_vsFilepath = path;
			m_vsSource = shaderSrc;
			break;
		case Shadertype::TESSELLATION_CONTROL:
			m_tsControlFilepath = path;
			m_tsControlSource = shaderSrc;
			break;
		case Shadertype::TESSELLATION_EVALUATION:
			m_tsEvalFilepath = path;
			m_tsEvalSource = shaderSrc;
			break;
		case Shadertype::GEOMETRY_SHADER:
			m_gsFilepath = path;
			m_gsSource = shaderSrc;
			break;
		case Shadertype::FRAGMENT_SHADER:
			m_fsFilepath = path;
			m_fsSource = shaderSrc;
			break;
		case Shadertype::COMPUTE_SHADER:
			m_csFilepath = path;
			m_csSource = shaderSrc;
			break;
		default:
			break;
		}
	}

	return this;
}

ShaderProgram* ShaderProgram::loadVertexShader(const std::string& path)
{
	return loadFromFile(path, Shadertype::VERTEX_SHADER);
}

ShaderProgram* ShaderProgram::loadTessellationShader(const std::string& tessControlPath, const std::string& tessEvalPath)
{
	return loadFromFile(tessControlPath, Shadertype::TESSELLATION_CONTROL)->loadFromFile(tessEvalPath, Shadertype::TESSELLATION_EVALUATION);
}

ShaderProgram* ShaderProgram::loadGeometryShader(const std::string& path)
{
	return loadFromFile(path, Shadertype::GEOMETRY_SHADER);
}

ShaderProgram* ShaderProgram::loadFragmentShader(const std::string& path)
{
	return loadFromFile(path, Shadertype::FRAGMENT_SHADER);
}

ShaderProgram* ShaderProgram::loadComputeShader(const std::string& path)
{
	return loadFromFile(path, Shadertype::COMPUTE_SHADER);
}

bool ShaderProgram::compile()
{
	if (m_program > 0)
	{
		glLinkProgram(m_program);
		bool linked = CheckProgramStatus(m_program, GL_LINK_STATUS);

		glValidateProgram(m_program);
		bool isValid = CheckProgramStatus(m_program, GL_VALIDATE_STATUS);

		if (linked && isValid)
			clearUniforms();

		return linked && isValid;
	}
	
	return false;
}

bool ShaderProgram::reloadShader()
{
	if (m_program > 0)
	{
		glDeleteProgram(m_program);
		m_program = 0;
	}

	m_program = glCreateProgram();

	if(!m_vsFilepath.empty())
	{
		std::string shaderSrc = ResourceLoader::loadShader(m_vsFilepath);
		shaderSrc = ResourceLoader::processIncludes(shaderSrc);

		if (!shaderSrc.empty() && m_vsSource != shaderSrc)
				m_vsSource = shaderSrc;

		const size_t vsShaderId = createShader(m_vsSource, Shadertype::VERTEX_SHADER);

		if(vsShaderId > 0)
			attachShader(vsShaderId);
	}

	if (!m_tsControlFilepath.empty())
	{
		std::string shaderSrc = ResourceLoader::loadShader(m_tsControlFilepath);
		shaderSrc = ResourceLoader::processIncludes(shaderSrc);

		if (!shaderSrc.empty() && m_tsControlSource != shaderSrc)
			m_tsControlSource = shaderSrc;

		const size_t shaderId = createShader(m_tsControlSource, Shadertype::TESSELLATION_CONTROL);
		if (shaderId > 0)
			attachShader(shaderId);
	}

	if (!m_tsEvalFilepath.empty())
	{
		std::string shaderSrc = ResourceLoader::loadShader(m_tsEvalFilepath);
		shaderSrc = ResourceLoader::processIncludes(shaderSrc);

		if (!shaderSrc.empty() && m_tsEvalSource != shaderSrc)
			m_tsEvalSource = shaderSrc;

		const size_t shaderId = createShader(shaderSrc, Shadertype::TESSELLATION_EVALUATION);
		if (shaderId > 0)
			attachShader(shaderId);
	}

	if (!m_gsFilepath.empty())
	{
		std::string shaderSrc = ResourceLoader::loadShader(m_gsFilepath);
		shaderSrc = ResourceLoader::processIncludes(shaderSrc);

		if (!shaderSrc.empty() && m_gsSource != shaderSrc)
			m_gsSource = shaderSrc;

		const size_t shaderId = createShader(shaderSrc, Shadertype::GEOMETRY_SHADER);
		if (shaderId > 0)
			attachShader(shaderId);
	}

	if (!m_fsFilepath.empty())
	{
		std::string shaderSrc = ResourceLoader::loadShader(m_fsFilepath);
		shaderSrc = ResourceLoader::processIncludes(shaderSrc);

		if (!shaderSrc.empty() && m_fsSource != shaderSrc)
			m_fsSource = shaderSrc;

		const size_t shaderId = createShader(shaderSrc, Shadertype::FRAGMENT_SHADER);
		if (shaderId > 0)
			attachShader(shaderId);
	}

	if (!m_csFilepath.empty())
	{
		std::string shaderSrc = ResourceLoader::loadShader(m_csFilepath);
		shaderSrc = ResourceLoader::processIncludes(shaderSrc);

		if (!shaderSrc.empty() && m_csSource != shaderSrc)
			m_csSource = shaderSrc;

		const size_t shaderId = createShader(shaderSrc, Shadertype::COMPUTE_SHADER);
		if (shaderId > 0)
			attachShader(shaderId);
	}

	return compile();
}

void ShaderProgram::bind()
{
	if(!m_isBound)
	{
		glUseProgram(m_program);
		m_isBound = true;
	}
}

void ShaderProgram::unbind()
{
	if(m_isBound)
	{
		glUseProgram(0);
		m_isBound = false;
	}
}

void ShaderProgram::setTexture(const std::string& samplerId, unsigned int textureId, int samplerSlot, unsigned int textureTarget)
{
	const int uniformLocation = getUniformLocation(samplerId);
	if (uniformLocation >= 0)
	{
		glActiveTexture(GL_TEXTURE0 + samplerSlot);
		glBindTexture(textureTarget, textureId);
		glUniform1i(uniformLocation, samplerSlot);
	}
}

void ShaderProgram::setTexture(const std::string& samplerId, Texture* texture, int samplerSlot)
{
	if (texture == nullptr)
		return;

	const int uniformLocation = getUniformLocation(samplerId);
	if (uniformLocation >= 0)
	{
		glActiveTexture(GL_TEXTURE0 + samplerSlot);
		glBindTexture(GL_TEXTURE_2D, texture->getId());
		glUniform1i(uniformLocation, samplerSlot);
	}
}

void ShaderProgram::setUniformi(const std::string& uniformId, const int& iv)
{
	const int uniformLocation = getUniformLocation(uniformId);

	if (uniformLocation >= 0)
		glUniform1i(uniformLocation, iv);
}

void ShaderProgram::setUniformf(const std::string& uniformId, const float& fv)
{
	const int uniformLocation = getUniformLocation(uniformId);

	if (uniformLocation >= 0)
		glUniform1f(uniformLocation, fv);
}

void ShaderProgram::setUniform(const std::string& uniformId, const glm::vec2& vector)
{
	const int uniformLocation = getUniformLocation(uniformId);

	if (uniformLocation >= 0)
		glUniform2f(uniformLocation, vector.x, vector.y);
}

void ShaderProgram::setUniform(const std::string& uniformId, const glm::vec3& vector)
{
	const int uniformLocation = getUniformLocation(uniformId);

	if (uniformLocation >= 0)
		glUniform3f(uniformLocation, vector.x, vector.y, vector.z);
}

void ShaderProgram::setUniform(const std::string& uniformId, const glm::vec4& vector)
{
	const int uniformLocation = getUniformLocation(uniformId);

	if (uniformLocation >= 0)
		glUniform4f(uniformLocation, vector.x, vector.y, vector.z, vector.w);
}

void ShaderProgram::setUniform(const std::string& uniformId, const glm::mat4& matrix)
{
	const int uniformLocation = getUniformLocation(uniformId);

	if (uniformLocation >= 0)
		glUniformMatrix4fv(uniformLocation, 1, false, &matrix[0][0]);
}

void ShaderProgram::setUniform(const std::string& uniformId, const int* list, size_t count)
{
	const int uniformLocation = getUniformLocation(uniformId);
	if (uniformLocation >= 0)
		glUniform1iv(uniformLocation, static_cast<GLsizei>(count), list);
}

void ShaderProgram::setUniform(const std::string& uniformId, const float* list, size_t count)
{
	const int uniformLocation = getUniformLocation(uniformId);
	if (uniformLocation >= 0)
		glUniform1fv(uniformLocation, static_cast<GLsizei>(count), list);
}

void ShaderProgram::setUniform(const std::string& uniformId, const glm::vec2* list, size_t count)
{
	const int uniformLocation = getUniformLocation(uniformId);
	if (uniformLocation >= 0)
		glUniform2fv(uniformLocation, static_cast<GLsizei>(count), glm::value_ptr(list[0]));
}

void ShaderProgram::setUniform(const std::string& uniformId, const glm::vec3* list, size_t count)
{
	const int uniformLocation = getUniformLocation(uniformId);
	if (uniformLocation >= 0)
		glUniform3fv(uniformLocation, static_cast<GLsizei>(count), glm::value_ptr(list[0]));
}

void ShaderProgram::setUniform(const std::string& uniformId, const glm::vec4* list, size_t count)
{
	const int uniformLocation = getUniformLocation(uniformId);
	if (uniformLocation >= 0)
		glUniform4fv(uniformLocation, static_cast<GLsizei>(count), glm::value_ptr(list[0]));
}

void ShaderProgram::setUniform(const std::string& uniformId, const glm::mat3* list, size_t count)
{
	const int uniformLocation = getUniformLocation(uniformId);
	if (uniformLocation >= 0)
		glUniformMatrix3fv(uniformLocation, static_cast<GLsizei>(count), GL_FALSE, glm::value_ptr(list[0]));
}

void ShaderProgram::setUniform(const std::string& uniformId, const glm::mat4* list, size_t count)
{
	const int uniformLocation = getUniformLocation(uniformId);
	if (uniformLocation >= 0)
		glUniformMatrix4fv(uniformLocation, static_cast<GLsizei>(count), GL_FALSE, glm::value_ptr(list[0]));
}

void ShaderProgram::setUniform(const std::string& uniformId, std::vector<int>& list)
{
	setUniform(uniformId, list.data(), list.size());
}

void ShaderProgram::setUniform(const std::string& uniformId, std::vector<float>& list)
{
	setUniform(uniformId, list.data(), list.size());
}

void ShaderProgram::setUniform(const std::string& uniformId, std::vector<glm::vec2>& list)
{
	setUniform(uniformId, list.data(), list.size());
}

void ShaderProgram::setUniform(const std::string& uniformId, std::vector<glm::vec3>& list)
{
	setUniform(uniformId, list.data(), list.size());
}

void ShaderProgram::setUniform(const std::string& uniformId, std::vector<glm::vec4>& list)
{
	setUniform(uniformId, list.data(), list.size());
}

void ShaderProgram::setUniform(const std::string& uniformId, std::vector<glm::mat3>& list)
{
	setUniform(uniformId, list.data(), list.size());
}

void ShaderProgram::setUniform(const std::string& uniformId, std::vector<glm::mat4>& list)
{
	setUniform(uniformId, list.data(), list.size());
}

size_t ShaderProgram::createShader(const std::string& shaderSource, Shadertype shadertype)
{
	if (shaderSource.empty() || shadertype == Shadertype::UNDEFINED)
		return false;

	GLuint shaderId = glCreateShader(getGlShadertype(shadertype));

	if (shaderId == 0)
	{
		std::cout << "[Shader]: creation failed - Could not find valid location when adding shader: " << getGlShadertype(shadertype) << std::endl;
		getchar();
		return 0;
	}

	char const* shaderSource_cstr = shaderSource.c_str();
	glShaderSource(shaderId, 1, &shaderSource_cstr, nullptr);
	glCompileShader(shaderId);

	const bool compiled = CheckCompileStatus(shaderId);

	if (compiled)
	{
		clearUniforms();
	}
	else
	{
		// compilation error occured, delete shader resource
		glDeleteShader(shaderId);
		shaderId = 0;
	}

	return shaderId;
}

void ShaderProgram::attachShader(const size_t shaderId) const
{
	if (m_program <= 0 || shaderId <= 0)
		return;

	glAttachShader(m_program, static_cast<GLuint>(shaderId));
	glDeleteShader(static_cast<GLuint>(shaderId));
}

int ShaderProgram::getUniformLocation(const std::string& uniformId)
{
	int location;
	if (m_uniformLocations.find(uniformId) != m_uniformLocations.end())
		location = m_uniformLocations[uniformId];
	else
	{
		std::string uniformName = uniformId;
		const int uniformLocation = glGetUniformLocation(m_program, uniformId.c_str());
		m_uniformLocations.insert(std::make_pair(uniformName, static_cast<int>(uniformLocation)));
		location = uniformLocation;
	}

	return location;
}

void ShaderProgram::clearUniforms()
{
	m_uniforms.clear();
	m_uniformLocations.clear();
}
