#include "AssimpLoader.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <glm/glm.hpp>
#include <iostream>

Drawable* AssimpLoader::loadObject(const std::string& file)
{
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(file, 0);

	if(!scene)
	{
		std::cout << "Could not load file: " << importer.GetErrorString() << std::endl;
		getchar();
		return nullptr;
	}

	const aiMesh* mesh = scene->mMeshes[0];
	if(!mesh)
	{
		std::cout << "No mesh found for: " << file << std::endl;
		getchar();
		return nullptr;
	}

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	std::vector<unsigned int> indices;

	for (unsigned i = 0; i < mesh->mNumVertices; i++)
	{
		aiVector3D vertex = mesh->mVertices[i];
		aiVector3D uv = mesh->mTextureCoords[0][i];
		aiVector3D normal = mesh->mNormals[i];

		vertices.push_back(glm::vec3(vertex.x, vertex.y, vertex.z));
		uvs.push_back(glm::vec2(uv.x, uv.y));
		normals.push_back(glm::vec3(normal.x, normal.y, normal.z));
	}

	unsigned int indexCount = 3 * mesh->mNumFaces;
	indices.reserve(indexCount);

	for (unsigned int i = 0; i < mesh->mNumFaces; i++)
	{
		indices.push_back(mesh->mFaces[i].mIndices[0]);
		indices.push_back(mesh->mFaces[i].mIndices[1]);
		indices.push_back(mesh->mFaces[i].mIndices[2]);
	}
	
	Drawable* drawable = new Drawable;
	drawable->AddVertices(vertices);
	drawable->AddTextureCoordinates(uvs);
	drawable->AddNormals(normals);
	drawable->AddIndices(indices);

	return drawable;
}
