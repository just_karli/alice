#ifndef MESH_H__
#define MESH_H__

#include <gl/glew.h>
#include <glm/glm.hpp>
#include <vector>
#include "ShaderProgram.h"

class Drawable
{
public:
	Drawable();
	~Drawable();

	void AddVertices(glm::vec3* vertices, size_t count);
	void AddVertices(std::vector<glm::vec3> vertices);

	void AddNormals(glm::vec3* normals, size_t count);
	void AddNormals(std::vector<glm::vec3> normals);

	void AddTextureCoordinates(glm::vec2* uvs, size_t count);
	void AddTextureCoordinates(std::vector<glm::vec2> texcoords);

	void AddIndices(unsigned int* indices, size_t count);
	void AddIndices(std::vector<unsigned int> indices);
	
	void LinkProgram(const ShaderProgram& shader);
	void Draw(const ShaderProgram& shader);
private:
	GLuint m_vbuffer = 0;
	GLuint m_ibuffer = 0;

	GLuint m_uvBuffer = 0;
	GLuint m_nBuffer = 0;

	GLint m_positionAttributeLocation = -1;
	GLint m_uvAttributeLocation = -1;
	GLint m_normalAttributeLocation = -1;

	size_t m_vertexCount;

	size_t m_stride;
	size_t m_size;
	size_t m_indicesCount;
};

#endif
