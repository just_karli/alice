#ifndef RANDOM_NUMBER_GENERATOR_H_
#define RANDOM_NUMBER_GENERATOR_H_
#pragma once

#include <random>
#include <glm/glm.hpp>
#include <glm/gtc/constants.inl>

class RandomNumberGenerator
{
public:
	static uint32_t XorShift32(uint32_t& state)
	{
		uint32_t x = state;
		x ^= x << 13;
		x ^= x >> 17;
		x ^= x << 15;
		state = x;
		return x;
	}

	static float RandomFloat01(uint32_t& state)
	{
		return (XorShift32(state) & 0xFFFFFF) / 16777216.0f;
	}

	static glm::vec3 RandomInUnitDisk(uint32_t& state)
	{
		glm::vec3 p;

		do
		{
			p = 2.0f * glm::vec3(RandomFloat01(state), RandomFloat01(state), RandomFloat01(state)) - glm::vec3(1, 1, 0);
		} while (glm::dot(p, p) >= 1.0);

		return p;
	}

	static glm::vec3 RandomInUnitSphere(uint32_t& state)
	{
		glm::vec3 p;
		do
		{
			p = 2.0f * glm::vec3(RandomFloat01(state), RandomFloat01(state), RandomFloat01(state)) - glm::vec3(1, 1, 1);
		} while (glm::dot(p, p) >= 1.0);

		return p;
	}

	static glm::vec3 RandomUnitVector(uint32_t& state)
	{
		float z = RandomFloat01(state) * 2.0f - 1.0f;
		float a = RandomFloat01(state) * 2.0f * glm::pi<float>();
		float r = glm::sqrt(1.0f - z * z);
		
		float x = r * glm::cos(a);
		float y = r * glm::sin(a);

		return glm::vec3(x, y, z);
	}
};

#endif 
