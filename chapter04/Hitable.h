#ifndef HITABLE_H_
#define HITABLE_H_
#pragma once

#include "Ray.h"
#include "Material.h"

class Hitable
{
public:
	virtual ~Hitable() { delete m_material; m_material = nullptr; };
	virtual bool intersect(const Ray& ray, float tMin, float tMax, HitRecord& record) const = 0;

protected:
	Material* m_material = nullptr;
};

#endif
