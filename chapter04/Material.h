#ifndef MATERIAL_H_
#define MATERIAL_H_
#pragma once

#include <glm/glm.hpp>
#include "RandomNumberGenerator.h"
#include "HitRecord.h"
#include "Ray.h"

struct Material
{
public:
	virtual ~Material() = default;
	virtual bool scatter(const Ray& ray, const HitRecord& hitrecord, glm::vec3& attenuation, Ray& scattered, uint32_t& state) const = 0;
};

class Lambertian : public Material
{
public:

	Lambertian() = default;
	Lambertian(const glm::vec3& color) : albedo(color) { }

	bool scatter(const Ray& ray, const HitRecord& hitrecord, glm::vec3& attenuation, Ray& scattered, uint32_t& state) const override
	{
		const glm::vec3 target = hitrecord.position + hitrecord.normal + RandomNumberGenerator::RandomUnitVector(state);

		scattered = Ray(hitrecord.position, target - hitrecord.position);
		attenuation = albedo;

		return true;
	}

	glm::vec3 albedo = glm::vec3(0.78f);
};

class Metal : public Material
{
public:
	Metal(const glm::vec3& color, const float roughness) : albedo(color), m_roughness(roughness) { }
	bool scatter(const Ray& ray, const HitRecord& hitrecord, glm::vec3& attenuation, Ray& scattered, uint32_t& state) const override
	{
		
		const glm::vec3 reflected = glm::reflect(glm::normalize(ray.direction), glm::normalize(hitrecord.normal));
		scattered = Ray(hitrecord.position, glm::normalize(reflected + m_roughness * RandomNumberGenerator::RandomUnitVector(state)));
		attenuation = albedo;
		return (glm::dot(scattered.direction, hitrecord.normal) > 0);
	}

	glm::vec3 albedo;
	float m_roughness;
};

class Dielectric : public Material
{
public:
	Dielectric(const float refractIdx) : refractionIndex(refractIdx) { }

	bool scatter(const Ray& ray, const HitRecord& hitrecord, glm::vec3& attenuation, Ray& scattered, uint32_t& state) const override
	{
		const glm::vec3 reflectDir = glm::reflect(glm::normalize(ray.direction), glm::normalize(hitrecord.normal));
		attenuation = glm::vec3(1);

		glm::vec3 outwardNormal;
		float niOverNt;
		float cosine;
		if (glm::dot(ray.direction, hitrecord.normal) > 0)
		{
			outwardNormal = -hitrecord.normal;
			niOverNt = refractionIndex;
			cosine = refractionIndex * glm::dot(glm::normalize(ray.direction), glm::normalize(hitrecord.normal));
		}
		else
		{
			outwardNormal = hitrecord.normal;
			niOverNt = 1 / refractionIndex;
			cosine = -glm::dot(glm::normalize(ray.direction), glm::normalize(hitrecord.normal));
		}

		glm::vec3 refractDir;
		float reflect_prob = 0.0f;
		
		if (refract(ray.direction, outwardNormal, niOverNt, refractDir))
			reflect_prob = schlick(cosine, refractionIndex);
		else
			reflect_prob = 1.0f;

		if (RandomNumberGenerator::RandomFloat01(state) < reflect_prob)
			scattered = Ray(hitrecord.position, glm::normalize(reflectDir));
		else
			scattered = Ray(hitrecord.position, glm::normalize(refractDir));

		return true;
	}

	bool refract(const glm::vec3& v, const glm::vec3& n, const float niOverNt, glm::vec3& refracted) const
	{
		const glm::vec3 uv = glm::normalize(v);
		const float dt = glm::dot(uv, n);

		const float discriminant = 1.0f - niOverNt * niOverNt * (1 - dt * dt);
		if (discriminant > 0)
		{
			refracted = niOverNt * (uv - n * dt) - n * glm::sqrt(discriminant);
			return true;
		}

		return false;
	}

	float schlick(const float cosine, const float refrIdx) const
	{
		float r0 = (1 - refrIdx) / (1 + refrIdx);
		r0 = r0 * r0;
		return r0 + (1 - r0) * glm::pow(1 - cosine, 5);
	}

private:
	float refractionIndex;
};

#endif 
