#include <iostream>
#include <glm/glm.hpp>
#include "spdlog/spdlog.h"

#include "Ray.h"
#include "HitRecord.h"
#include "Hitable.h"
#include "Sphere.h"
#include "Camera.h"
#include <random>

// returns blue environment background dependent on the y direction if there was no hit with a world object
// else it will return the surface normal of the object
glm::vec3 Trace(const Ray& r, std::vector<std::unique_ptr<Hitable>>& world)
{
	HitRecord nearestHit;
	float closestHitDistance = std::numeric_limits<float>::max();
	bool hitAnything = false;
	for(const auto& worldObject : world)
	{
		if(worldObject->intersect(r, 0.0f, closestHitDistance, nearestHit))
		{
			hitAnything = true;
			closestHitDistance = nearestHit.t;
		}
	}

	if(hitAnything)
		return 0.5f * (nearestHit.normal + 1.0f);
	
	glm::vec3 unit_dir = glm::normalize(r.direction);
	float t = 0.5f * (unit_dir.y + 1.0f);
	return (1.0f - t) * glm::vec3(1.0f) + t * glm::vec3(0.5f, 0.7f, 1.0f);
}

/**
* Chapter 4: Anti aliasing example
*/
int main(int argc, char** argv)
{
	auto console = spdlog::stdout_color_mt("console");
	int nx = 200;
	int ny = 100;
	int ns = 100;

	// the ppm header 
	// format name, width, height, #colors
	std::cout << "P3\n" << nx << " " << ny << std::endl;
	std::cout << 255 << std::endl;

	std::vector<std::unique_ptr<Hitable>> world;

	world.push_back(std::make_unique<Sphere>(glm::vec3(0, 0, -1.0f), 0.5, nullptr));
	world.push_back(std::make_unique<Sphere>(glm::vec3(0, -100.5, -1.0f), 100, nullptr));

	Camera* cam = new Camera(glm::vec3(0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0), 90.0f, nx / static_cast<float>(ny));
	
	for (int j = ny - 1; j >= 0; j--)
	{
		uint32_t state = (j * 9781) | 1;

		for (int i = 0; i < nx; i++)
		{
			glm::vec3 col(0.0f);
			for(int s = 0; s < ns; s++)
			{
				float u = float(i + RandomNumberGenerator::RandomFloat01(state)) / float(nx);
				float v = float(j + RandomNumberGenerator::RandomFloat01(state)) / float(ny);

				Ray r = cam->getRay(u, v, state);
				col += Trace(r, world);
			}
			

			col /= float(ns);

			int ir = int(255.99 * col.r);
			int ig = int(255.99 * col.g);
			int ib = int(255.99 * col.b);

			std::cout << ir << " " << ig << " " << ib << std::endl;
		}
	}

	delete cam;

	return 0;
}