#include <iostream>
#include "spdlog/spdlog.h"

int main(int argc, char** argv)
{
	auto console = spdlog::stdout_color_mt("console");
	spdlog::get("console")->info("Hello Logger");
	std::cout << "Hello Alice!" << std::endl;
	getchar();
	return 0;
}