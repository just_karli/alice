#ifndef CAMERA_H_
#define CAMERA_H_

#pragma once
#include <glm/glm.hpp>
#include "Ray.h"
#include "RandomNumberGenerator.h"

struct Camera
{
	Camera(const glm::vec3 _position, const glm::vec3 lookAt, const glm::vec3 up, const float verticalFov, const float aspect)
	{
		float theta = glm::radians(verticalFov);
		const float halfHeight = glm::tan(theta * 0.5f);
		const float halfWidth = aspect * halfHeight;

		position = _position;
		glm::vec3 w = glm::normalize(position - lookAt);
		glm::vec3 u = glm::normalize(glm::cross(up, w));
		glm::vec3 v = glm::cross(w, u);

		lowerLeftCorner = position - halfWidth * u - halfHeight * v - w;
		horizontal = 2 * halfWidth * u;
		vertical = 2 * halfHeight * v;
	}

	Camera(const glm::vec3 _position, const glm::vec3 lookAt, const glm::vec3 up, const float verticalFov, const float aspect, float aperture, float focusDistance)
	{
		aperture = aperture * 0.5f;
		float theta = glm::radians(verticalFov);

		const float halfHeight = glm::tan(theta * 0.5f);
		const float halfWidth = aspect * halfHeight;

		position = _position;
		m_forward = glm::normalize(position - lookAt);
		m_right = glm::normalize(glm::cross(up, m_forward));
		m_up = glm::cross(m_forward, m_right);

		lowerLeftCorner = position - halfWidth * focusDistance * m_right - halfHeight * focusDistance * m_up - focusDistance * m_forward;
		horizontal = 2 * halfWidth * focusDistance * m_right;
		vertical = 2 * halfHeight * focusDistance * m_up;
	}

	Ray getRay(const float s, const float t, uint32_t& state) const
	{
		const glm::vec3 rd = m_aperture * RandomNumberGenerator::RandomInUnitDisk(state);
		const glm::vec3 offset = m_right * rd.x + m_up * rd.y;
		return Ray(position + offset, glm::normalize(lowerLeftCorner + s * horizontal + t * vertical - position - offset));
	}



	glm::vec3 position;
	glm::vec3 lowerLeftCorner;
	glm::vec3 horizontal;
	glm::vec3 vertical;

	glm::vec3 m_right, m_up, m_forward;

	float m_aperture = 0.0f;
};

#endif 
