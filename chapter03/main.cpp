#include <iostream>
#include <glm/glm.hpp>
#include "spdlog/spdlog.h"

#include "Ray.h"
#include "HitRecord.h"
#include "Hitable.h"
#include "Sphere.h"

// returns blue environment background dependent on the y direction if there was no hit with a world object
// else it will return the surface normal of the object
glm::vec3 Trace(const Ray& r, std::vector<std::unique_ptr<Hitable>>& world)
{
	HitRecord nearestHit;
	float closestHitDistance = std::numeric_limits<float>::max();
	bool hitAnything = false;
	for(const auto& worldObject : world)
	{
		if(worldObject->intersect(r, 0.0f, closestHitDistance, nearestHit))
		{
			hitAnything = true;
			closestHitDistance = nearestHit.t;
		}
	}

	if(hitAnything)
		return 0.5f * (nearestHit.normal + 1.0f);
	
	glm::vec3 unit_dir = glm::normalize(r.direction);
	float t = 0.5f * (unit_dir.y + 1.0f);
	return (1.0f - t) * glm::vec3(1.0f) + t * glm::vec3(0.5f, 0.7f, 1.0f);
}

/**
* Chapter 3: 
* Print an rgb image in the ppm format to the stdout 
*/
int main(int argc, char** argv)
{
	auto console = spdlog::stdout_color_mt("console");
	int nx = 200;
	int ny = 100;

	// the ppm header 
	// format name, width, height, #colors
	std::cout << "P3\n" << nx << " " << ny << std::endl;
	std::cout << 255 << std::endl;

	glm::vec3 lowerLeftCorner(-2.0f, -1.0f, -1.0f);
	glm::vec3 horizontal(4.0f, 0.0f, 0.0f);
	glm::vec3 vertical(0.0f, 2.0f, 0.0f);
	glm::vec3 origin(0.0f);

	std::vector<std::unique_ptr<Hitable>> hitableList;

	hitableList.push_back(std::make_unique<Sphere>(glm::vec3(0, 0, -1.0f), 0.5, nullptr));
	hitableList.push_back(std::make_unique<Sphere>(glm::vec3(0, -100.5, -1.0f), 100, nullptr));

	for (int j = ny - 1; j >= 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			float u = float(i) / float(nx);
			float v = float(j) / float(ny);
			const glm::vec3 direction = lowerLeftCorner + u * horizontal + v * vertical;
			const Ray ray(origin, direction);

			const glm::vec3 col = Trace(ray, hitableList);

			int ir = int(255.99 * col.r);
			int ig = int(255.99 * col.g);
			int ib = int(255.99 * col.b);

			std::cout << ir << " " << ig << " " << ib << std::endl;
		}
	}

	return 0;
}