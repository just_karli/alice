#ifndef SPHERE_H_
#define SPHERE_H_
#pragma once

#include "Hitable.h"

class Sphere : public Hitable
{
public: 
	Sphere() = default;
	~Sphere() override = default;

	Sphere(glm::vec3& center, const float radius, Material* material) : m_center(center), m_radius(radius)
	{
		m_material = material;
	}

	bool intersect(const Ray& ray, float tMin, float tMax, HitRecord& record) const override;

	glm::vec3 getCenter() const { return m_center; }
	float getRadius() const { return m_radius; }
private: 
	glm::vec3 m_center;
	float m_radius;
};

#endif 
