#include "Sphere.h"
#include "HitRecord.h"

bool Sphere::intersect(const Ray& ray, float tMin, float tMax, HitRecord& record) const
{
	glm::vec3 oc = ray.position - m_center;

	// build quadratic equation
	float a = glm::dot(ray.direction, ray.direction);	// direction * direction 
	float b = glm::dot(oc, ray.direction);
	float c = glm::dot(oc, oc) - m_radius * m_radius;


	float discriminant = b * b - a * c;

	if(discriminant > 0)
	{
		// ray - sphere hit

		// evaluate both quadratic characteristics
		float x1 = (-b - glm::sqrt(b * b - a * c)) / a;
		if((x1 < tMax && x1 > tMin))
		{
			record.t = x1;
			record.position = ray.positionAtParameter(record.t);
			record.normal = (record.position - m_center) / m_radius;
			record.material = m_material;
			return true;
		}

		float x2 = (-b + glm::sqrt(b * b - a * c)) / a;

		if((x2 < tMax && x2 > tMin))
		{
			record.t = x2;
			record.position = ray.positionAtParameter(record.t);
			record.normal = (record.position - m_center) / m_radius;
			record.material = m_material;
			return true;
		}
	}

	return false;
}
