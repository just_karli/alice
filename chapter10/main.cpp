#include <iostream>
#include <glm/glm.hpp>
#include "spdlog/spdlog.h"

#include "Ray.h"
#include "Hitable.h"
#include "Sphere.h"
#include "Camera.h"
#include <random>
#include "RandomNumberGenerator.h"
#include "Material.h"


// returns blue environment background dependent on the y direction if there was no hit with a world object
// else it will return the surface normal of the object
glm::vec3 Trace(const Ray& r, std::vector<std::unique_ptr<Hitable>>& world, const int depth, uint32_t& state)
{
	HitRecord nearestHit;
	float closestHitDistance = std::numeric_limits<float>::max();
	bool hitAnything = false;
	for(const auto& worldObject : world)
	{
		if(worldObject->intersect(r, 0.01f, closestHitDistance, nearestHit))
		{
			hitAnything = true;
			closestHitDistance = nearestHit.t;
		}
	}

	if (hitAnything)
	{
		Ray scatteredRay;
		glm::vec3 attenuation;
		if(depth < 50 && nearestHit.material->scatter(r, nearestHit, attenuation, scatteredRay, state))
		{
			return attenuation * Trace(scatteredRay, world, depth + 1, state);
		}

		return glm::vec3(0.0f);
	}

	const glm::vec3 unit_dir = glm::normalize(r.direction);
	float t = 0.5f * (unit_dir.y + 1.0f);
	return (1.0f - t) * glm::vec3(1.0f) + t * glm::vec3(0.5f, 0.7f, 1.0f);
}

std::vector<std::unique_ptr<Hitable>> GenerateRandomScene(uint32_t& state)
{
	int n = 500;
	std::vector<std::unique_ptr<Hitable>> hitableList;
	hitableList.push_back(std::make_unique<Sphere>(glm::vec3(0, -1000, 0), 1000, new Lambertian(glm::vec3(0.5, 0.5, 0.5))));

	int i = 1;
	for(int a = -11; a < 11; a++)
	{
		for(int b = -11; b < 11; b++)
		{
			float chooseMaterial = RandomNumberGenerator::RandomFloat01(state);
			glm::vec3 center(
				a + 0.9 * RandomNumberGenerator::RandomFloat01(state),
				0.2,
				b + 0.9 * RandomNumberGenerator::RandomFloat01(state)
			);

			if(glm::length(center - glm::vec3(4, 0.2, 0)) > 0.9f)
			{
				if(chooseMaterial < 0.8f)
				{
					// diffuse
					hitableList.push_back(std::make_unique<Sphere>(center, 0.2, new Lambertian(
						glm::vec3(
							RandomNumberGenerator::RandomFloat01(state) * RandomNumberGenerator::RandomFloat01(state),
							RandomNumberGenerator::RandomFloat01(state) * RandomNumberGenerator::RandomFloat01(state),
							RandomNumberGenerator::RandomFloat01(state) * RandomNumberGenerator::RandomFloat01(state))
					)));
				}
				else if (chooseMaterial < 0.95f)
				{
					// metal
					hitableList.push_back(std::make_unique<Sphere>(center, 0.2, new Metal(
						glm::vec3(
							0.5 * (1 + RandomNumberGenerator::RandomFloat01(state)),
							0.5 * (1 + RandomNumberGenerator::RandomFloat01(state)),
							0.5 * (1 + RandomNumberGenerator::RandomFloat01(state))
						),
						0.5 * RandomNumberGenerator::RandomFloat01(state)
					)));

				}
				else
				{
					// glass
					hitableList.push_back(std::make_unique<Sphere>(center, 0.2, new Dielectric(1.5)));
				}
			}
		}
	}

	hitableList.push_back(std::make_unique<Sphere>(glm::vec3(0, 1, 0), 1.0, new Dielectric(1.5)));
	hitableList.push_back(std::make_unique<Sphere>(glm::vec3(-4, 1, 0), 1.0, new Lambertian(glm::vec3(0.4, 0.2, 0.1))));
	hitableList.push_back(std::make_unique<Sphere>(glm::vec3(4, 1, 0), 1.0, new Metal(glm::vec3(0.7, 0.6, 0.5), 0.0f)));
	return hitableList;
}

/**
* Chapter 10: Camera with defocused blur
*/
int main(int argc, char** argv)
{
	auto console = spdlog::stdout_color_mt("console");
	int nx = 200;
	int ny = 100;
	int ns = 100;

	// the ppm header 
	// format name, width, height, #colors
	std::cout << "P3\n" << nx << " " << ny << std::endl;
	std::cout << 255 << std::endl;

	uint32_t x32 = 314159265;

	std::vector<std::unique_ptr<Hitable>> world = GenerateRandomScene(x32);


	const glm::vec3 lookPosition = glm::vec3(0, 0, 0);
	const glm::vec3 lookAt = glm::vec3(0, 0, -1);

	const float distanceToFocus = glm::length(lookPosition - lookAt);
	const float aperture = 2.0f;

	Camera cam(lookPosition, lookAt, glm::vec3(0, 1, 0), 90.0f, nx / ny, aperture, distanceToFocus);

	for (int j = ny - 1; j >= 0; j--)
	{
		uint32_t state = (j * 9781) | 1;

		for (int i = 0; i < nx; i++)
		{
			glm::vec3 col(0.0f);
			for(int s = 0; s < ns; s++)
			{
				float u = float(i + RandomNumberGenerator::RandomFloat01(state)) / float(nx);
				float v = float(j + RandomNumberGenerator::RandomFloat01(state)) / float(ny);

				Ray r = cam.getRay(u, v, state);
				col += Trace(r, world, 0, state);
			}

			col /= float(ns);

			int ir = int(255.99 * col.r);
			int ig = int(255.99 * col.g);
			int ib = int(255.99 * col.b);

			std::cout << ir << " " << ig << " " << ib << std::endl;
		}
	}

	return 0;
}
